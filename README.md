# Givappway
[![pipeline status](https://git.thm.de/fmdr54/givappway/badges/main/pipeline.svg)](https://git.thm.de/fmdr54/givappway/-/commits/main)
## Einleitung
Das Projekt "Givappway" ist im Rahmen der Prüfungsleistung des Moduls "Entwicklung mobiler Applikationen" im Wintersemester 2021/22 entstanden. Die Projektgruppe besteht aus: **Janek Berg, Florian Dörr, Karsten Krause und Marius von Egan-Krieger**

Ziel war die Entwicklung einer Applikation, mit der Kartons mit der Aufschrift "Zu verschenken" digitalisiert werden können - Nutzer sollen über Kartons in ihrer Nähe informiert werden und schnell Informationen zu diesen einholen können. Somit wird das Problem gelöst, dass solche Kartons oft gar nicht wahrgenommen werden und es erhalten damit deutlich mehr alte Gegenstände ein neues Leben. Der Ansatz der App ist dabei eine möglichst einfache und anonyme Gestaltung des Prozesses.

## Installation und Entwicklung
### Vorbereitung
Bevor die Applikation genutzt werden kann müssen die benötigten externen Dienste bereitgestellt werden:
* Ein Firebase-Projekt, in welchen **Authentication**, **Firestore** und **Storage** aktiviert sind.
  * Die Erstellung eines solchen Projektes kann [https://firebase.google.com/](https://firebase.google.com/) nach einer Registrierung vorgenommen werden. Für die Testentwicklung genügt der kostenfreie **"Spark-Plan"**.
  * Die benötigten Daten für den API-Zugriff findet man unter **Projekteinstellungen -> Allgemein -> Meine Apps**.
  * Sinnvoll ist es, die von uns angelegten **Firebase Rules** für **Cloud Firestore** und **Cloud Storage** zu übernehmen, welche als `emulatorConfig/firestore.rules` bzw. `emulatorConfig/storage.rules` verfügbar sind.
* Ein Konto bei **OpenCage**, welches für die API zur Umrechnung zwischen Adressen und Standorten in beide Richtungen genutzt wird.
  * Die Erstellung erfolgt unter [https://opencagedata.com/](https://opencagedata.com/), die **"Free Trial"**-Variante bietet 2.500 Requests pro Tag, jedoch nur zur Testentwicklung. Wir betrachten dieses Hochschulprojekt als solche, weshalb die API geeignet ist.
  * Im Dashboard des eigenen Accounts kann der API-Key abgerufen werden.
* Ein Konto bei **OpenWeather**, welches die API zur Prüfung des Wetters am Standort bereitstellt.
  * Die Registrierung erfolgt unter [https://openweathermap.org/](https://openweathermap.org/), auch hier genügt die **"Free"**-Option mit 1.000.000 Requests pro Monat.

Die Daten der Dienste müssen als `src/environments/credentials.json` bereitgestellt werden. Die Template-Datei gibt dabei die Struktur vor:
```json
{
  "firebase": {
    "projectId": "givappway",
    "appId": "X:XXXXX:web:XXX",
    "storageBucket": "givappway.appspot.com",
    "locationId": "europe-west",
    "apiKey": "FIREBASE_APIKEY",
    "authDomain": "givappway.firebaseapp.com",
    "messagingSenderId": "SENDERID"
  },
  "openCage": {
    "apiKey": "POSITIONSTACK_APIKEY"
  },
  "openweather": "OPEN_WEATHER_APIKEY"
}
```

### Installation
Bevor das Projekt gestartet werden kann müssen alle Dependencies installiert werden:
```bash
npm install
# Alternativ ohne Development-Dependencies:
npm install --production
```
Anschließend steht die Applikation zur Verwendung bereit.

### Setup für Google-Login auf nativen Plattformen
#### Android
Vorbereitend ist es notwendig, den SHA1-Fingerprint des Zertifikats, mit dem die App signiert wird zu ermitteln. Google liefert eine [Anleitung](https://developers.google.com/android/guides/client-auth), wir empfehlen folgendes Vorgehen:
1. Android-Projekt öffnen lassen: `ionic capacitor open android`
2. In Android-Studio das Terminal öffnen und folgenden Befehl ausführen: `gradlew signingReport`
  * Dort muss der Eintrag mit **Variant: debug** und **Config: debug** gesucht werden - dort kann dann der SHA1-Wert entnommen werden.

In Firebase muss dem Projekt unter "Projekteinstellungen" eine neue Android-App hinzugefügt werden. In der Konfiguration wird für diese der Paketname `de.thm.ema.givappway` eingetragen. Außerdem wird der SHA1-Wert, der zuvor ermittelt wurde.
* Später können der App unter "Projekteinstellungen" weitere SHA1-Fingerprints hinzugefügt werden. Das ist notwendig, da dieser für jedes System individuell ist, sodass bei einem Build eines anderen Entwicklers auch ein anderer Fingerprint hinterlegt werden muss.

Anschließend bietet Firebase eine `google-services.json` zum Download an. Diese muss heruntergeladen und im Projekt als `android/app/google-services.json` hinterlegt werden.

Anschließend kann unter Android der Google-Login genutzt werden.

#### iOS
In Firebase muss dem Projekt wieder unter "Projekteinstellungen" eine neue App hinzugefügt werden, dieses Mal vom Typ iOS. Als Bundle-ID wird `de.thm.ema.givappway` eingegeben.

Nach dem Registrieren der App bietet Firebase eine `GoogleService-Info.plist` zum Download an, welche im Projekt als `ios/App/App/GoogleService-Info.plist` im Projekt abgelegt werden muss.

Nachdem das Projekt mit `ionic capacitor open ios` in Xcode geöffnet wurde müssen die in [dieser Anleitung](https://github.com/robingenz/capacitor-firebase-authentication/blob/main/docs/setup-google.md#ios) unter Punkt 2 beschriebenen Schritte befolgt werden.

Anschließend kann unter iOS der Google-Login genutzt werden.

### Verwendung
Die Applikation kann über folgenden Befehl gestartet werden:
```bash
npm start
```
Anschließend ist sie im Browser unter `localhost` über den angezeigten Port verfügbar (standardmäßig `8100`).

#### Tests
Aus Zeitgründen wurden keine umfangreichen Tests für das Projekt geschrieben, es wurde aber die Konfiguration aller Tests angepasst, sodass diese mit allen genutzten Integrationen (z.B. Router und Firebase) funktioniert. Damit besteht die Grundlage für das Einfügen von Tests ohne sich mit deren Konfiguration auseinandersetzen zu müssen. Die Tests werden mit der **Firebase Local Emulator Suite** ausgeführt, welche eine lokale Firebase-Instanz emuliert. Auf diese Weise beeinträchtigen die Tests nicht die eigentliche Instanz und benötigen auch keine der beschränkten Abfragen. Sie können in zwei Modi gestartet werden:
```bash
# Interaktiv
npm test
# non-interaktiv (CI)
npm test:ci
```
* Der **interaktive** Testmodus startet den Test mit allen Browsern (Firefox, Chrome, Firefox Headless, Chrome Headless, Chrome Headless CI) und im `watch`-Modus. Durch diesen bleiben die Tests nach Ausführung geöffnet, sodass die Ergebnisse im Testbrowser analysiert werden können. Änderungen des Codes lösen erneute Durchläufe aus.
  * Dieser Modus ist vor allem für die Entwicklung und das Debugging geeignet.
* Der **non-interaktive** Testmodus startet lediglich den Chrome Headless Browser und führt alle Tests einmalig aus.
  * Dieser Modus ist für die Continuous Integration vorgesehen, kann aber auch für schnelle einzelne Testdurchläufe lokal verwendet werden.

## Dokumentation
Die Projektdokumentation wurde als Wiki dieses Repositories angelegt und kann dort eingesehen werden: [https://git.thm.de/fmdr54/givappway/-/wikis/home](https://git.thm.de/fmdr54/givappway/-/wikis/home)
