import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'de.thm.ema.givappway',
  appName: 'givappway',
  webDir: 'www',
  bundledWebRuntime: false,
  plugins: {
    // eslint-disable-next-line @typescript-eslint/naming-convention
    FirebaseAuthentication: {
      // Disable native auth layer to get credential for web layer
      skipNativeAuth: true,
      providers: ['google.com'],
    },
  },
};

export default config;
