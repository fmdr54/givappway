import credentials from './credentials.json';
const { firebase, openCage, openweather } = credentials;
import { EnvironmentInterface } from './environmentInterface';

export const environment: EnvironmentInterface = {
  firebase,
  openCage: openCage.apiKey,
  openweather,
  production: true,
  useEmulators: false,
};
