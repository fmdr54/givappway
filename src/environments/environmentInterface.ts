export interface EnvironmentInterface {
  firebase: any;
  openCage: string;
  openweather: string;
  production: boolean;
  useEmulators: boolean;
}
