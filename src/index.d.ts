import { Timestamp } from '@angular/fire/firestore';
export type Validation = {
  [key: string]: {
    type:
      | 'required'
      | 'email'
      | 'max'
      | 'maxLength'
      | 'min'
      | 'minLength'
      | 'pattern'
      | 'wrongPassphrase'
      | 'offline';
    message: string;
  }[];
};

export type ImageReference = {
  imageReference: string;
  timestamp: number;
};

export type LoadedImageReference = {
  url: string;
} & ImageReference;
