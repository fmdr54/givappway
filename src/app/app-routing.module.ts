import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: () =>
      import('./pages/navigationbar/navigationbar.module').then(
        (m) => m.NavigationbarPageModule
      ),
  },
  {
    path: 'login',
    loadChildren: () =>
      import('./pages/login/login.module').then((m) => m.LoginPageModule),
  },
  {
    path: 'registration',
    loadChildren: () =>
      import('./pages/registration/registration.module').then(
        (m) => m.RegistrationPageModule
      ),
  },
  {
    path: 'details/:id',
    loadChildren: () =>
      import('./pages/details/details.module').then((m) => m.DetailsPageModule),
  },
  {
    path: 'question-answer/:id',
    loadChildren: () =>
      import('./pages/question-answer/question-answer.module').then(
        (m) => m.QuestionAnswerPageModule
      ),
  },
  {
    path: 'withdrawal',
    loadChildren: () =>
      import('./pages/withdrawal/withdrawal.module').then(
        (m) => m.WithdrawalPageModule
      ),
  },
  {
    path: 'my-questions',
    loadChildren: () =>
      import('./pages/my-questions/my-questions.module').then(
        (m) => m.MyQuestionsPageModule
      ),
  },
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
