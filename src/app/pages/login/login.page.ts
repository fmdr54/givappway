import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '../../services/authentication.service';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { User } from '../../data/User';
import {
  AlertController,
  IonInput,
  LoadingController,
  NavController,
} from '@ionic/angular';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  public validationForm: FormGroup;
  validationMessages: any;
  private user: User;

  constructor(
    private navController: NavController,
    private auth: AuthenticationService,
    private formBuilder: FormBuilder,
    private loadingController: LoadingController,
    private alertController: AlertController
  ) {
    this.prepareFormValidation();
  }

  ngOnInit() {}

  /**
   * Method that logs in a user with email and password.
   *
   * @param email The email address of a user.
   * @param password The password of a user.
   */
  public async logIn(email: IonInput, password: IonInput) {
    const loading = await this.loadingController.create();
    try {
      await loading.present();
      await this.auth.signIn(email.value as string, password.value as string);
      await loading.dismiss();
      // this has to route to 'home'
      await this.navController.navigateRoot('/home');
    } catch (error) {
      const alert = await this.alertController.create({
        header: 'Login Fehlgeschlagen',
        message: error.message,
        buttons: ['OK'],
      });
      await loading.dismiss();
      await alert.present();
      console.log(error);
    }
  }

  /**
   * Method that logs a user in with his google account.
   */
  public async loginWithGoogle() {
    const loading = await this.loadingController.create();
    try {
      const result = await this.auth.signInWithGoogle();
      await loading.present();
      if (result.user.uid) {
        //since google login always creates new objects, get the old object and copy its data
        let oldObject = await this.auth.getUserById(result.user.uid);
        //if there is no old object, create an empty one
        if (oldObject === undefined) {
          oldObject = new User('', '', '', '', null);
        }
        this.user = new User(
          result.user.uid,
          oldObject.street,
          oldObject.postalCode,
          oldObject.city,
          oldObject.location,
          oldObject.lastNotificationCheck
        );
        await this.auth.saveUserDetails(this.user);
      }
      await loading.dismiss();
      // this has to route to 'home'
      await this.navController.navigateRoot('/home');
    } catch (error) {
      const alert = await this.alertController.create({
        header: 'Login mit Google fehlgeschlagen',
        message: error.message,
        buttons: ['OK'],
      });
      await loading.dismiss();
      await alert.present();
      console.log(error);
    }
  }

  /**
   * Method that logs a user out.
   */
  public async logOut() {
    const loading = await this.loadingController.create();
    try {
      await loading.present();
      await this.auth.signOut();
      await loading.dismiss();
    } catch (error) {
      const alert = await this.alertController.create({
        header: 'Logout Fehlgeschlagen',
        message: error.message,
        buttons: ['OK'],
      });
      await loading.dismiss();
      await alert.present();
      console.log(error);
    }
  }

  /**
   * Method that navigates the user to the main app without logging the user in.
   */
  public async discover() {
    // this has to route to 'home'
    await this.navController.navigateRoot('/home');
  }

  /**
   * Method that navigates to the registration page.
   */
  public async navigateToRegistration() {
    await this.navController.navigateForward('/registration');
  }

  /***
   * Method that configures the validation for the email input and password input.
   */
  private prepareFormValidation() {
    this.validationForm = this.formBuilder.group({
      email: new FormControl(
        '',
        Validators.compose([Validators.required, Validators.email])
      ),
      password: new FormControl('', Validators.compose([Validators.required])),
    });

    this.validationMessages = {
      email: [
        { type: 'required', message: 'Email erforderlich' },
        { type: 'email', message: 'Dies ist keine gültige E-Mail-Adresse' },
      ],
      password: [{ type: 'required', message: 'Passwort erforderlich' }],
    };
  }
}
