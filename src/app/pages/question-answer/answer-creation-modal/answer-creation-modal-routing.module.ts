import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AnswerCreationModalPage } from './answer-creation-modal.page';

const routes: Routes = [
  {
    path: '',
    component: AnswerCreationModalPage,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AnswerCreationModalPageRoutingModule {}
