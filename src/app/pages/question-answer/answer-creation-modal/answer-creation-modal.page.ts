import { Component, OnInit, ViewChild } from '@angular/core';
import { ModalController, NavParams, ToastController } from '@ionic/angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CdkStepper } from '@angular/cdk/stepper';
import { Timestamp } from '@angular/fire/firestore';
import { Answer } from '../../../data/Answer';
import { QuestionAnswerService } from '../../../services/question-answer.service';
import { presentToast } from 'src/app/util/toast';
import { AuthenticationService } from '../../../services/authentication.service';
import { Carton } from '../../../data/Carton';
import { CartonsService } from '../../../services/cartons.service';
import { Validation } from 'src';
import { User } from '../../../data/User';

@Component({
  selector: 'app-answer-creation-modal',
  templateUrl: './answer-creation-modal.page.html',
  styleUrls: ['./answer-creation-modal.page.scss'],
})
export class AnswerCreationModalPage implements OnInit {
  @ViewChild('cdkStepper')
  cdkStepper: CdkStepper;

  public passphraseForm: FormGroup;
  public passphraseValidationMessages: Validation;
  public answerForm: FormGroup;
  public answerValidationMessages: Validation;
  private carton: Carton;
  private user: User;

  constructor(
    private authenticationService: AuthenticationService,
    private cartonsService: CartonsService,
    private modalController: ModalController,
    private toastController: ToastController,
    private formBuilder: FormBuilder,
    private questionAnswerService: QuestionAnswerService,
    private navigationParameter: NavParams
  ) {
    // initial loading of user
    this.user = this.authenticationService.getCurrentUser();
    // subscribe to the user observable
    this.authenticationService
      .getUserObservable()
      .subscribe((user) => (this.user = user));
    this.passphraseForm = this.formBuilder.group({
      passphrase: ['', Validators.compose([Validators.required])],
    });

    this.passphraseValidationMessages = {
      passphrase: [
        { type: 'required', message: 'Die Passphrase ist erforderlich.' },
        { type: 'wrongPassphrase', message: 'Die Passphrase ist falsch.' },
      ],
    };

    this.answerForm = this.formBuilder.group({
      answer: ['', Validators.compose([Validators.required])],
    });

    this.answerValidationMessages = {
      answer: [{ type: 'required', message: 'Eine Antwort ist erforderlich.' }],
    };
  }

  async ngOnInit() {
    //get the carton from the firestore
    this.carton = await this.cartonsService.getCartonByID(
      this.navigationParameter.data.cartonID
    );
  }

  /**
   * Closes this modal by dismissing it.
   */
  public async close() {
    await this.modalController.dismiss();
  }

  /**
   * Validates the passphrase given by the user by comparing
   * it to the passphrase of the carton this modal refers to.
   * Proceeds to the next stepper step on success, sets form error
   * on failure.
   */
  public async validatePassphrase() {
    const { passphrase } = this.passphraseForm.value;
    if (passphrase.toLocaleLowerCase() !== this.carton.passphrase) {
      this.passphraseForm.get('passphrase').setErrors({
        ...this.passphraseForm.get('passphrase').errors,
        wrongPassphrase: true,
      });
      return;
    }
    //if the check didn't fail, go to the next step
    this.cdkStepper.next();
  }

  /**
   * Handles the submit action by getting the text of the created
   * answer and pushing a new answer object/document to firestore.
   */
  public async onSubmit() {
    if (this.user === undefined) {
      return;
    }
    const answerText = this.answerForm.get('answer').value;
    //create new answer
    const answer = new Answer(
      this.user.id,
      this.navigationParameter.data.question,
      answerText,
      Timestamp.now()
    );
    //push it to firestore collection
    await this.questionAnswerService.addAnswer(answer);
    //dismiss the modal and present a toast
    await this.modalController.dismiss();
    await presentToast(this.toastController, 'Die Antwort wurde erstellt!');
  }
}
