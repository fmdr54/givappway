import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AnswerCreationModalPageRoutingModule } from './answer-creation-modal-routing.module';

import { AnswerCreationModalPage } from './answer-creation-modal.page';
import { StepperComponentModule } from '../../../components/stepper/stepper.module';
import { CdkStepperModule } from '@angular/cdk/stepper';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AnswerCreationModalPageRoutingModule,
    StepperComponentModule,
    ReactiveFormsModule,
    CdkStepperModule,
  ],
  declarations: [AnswerCreationModalPage],
})
export class AnswerCreationModalPageModule {}
