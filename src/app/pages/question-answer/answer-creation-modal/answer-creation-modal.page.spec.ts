import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule, NavParams } from '@ionic/angular';

import { AnswerCreationModalPage } from './answer-creation-modal.page';
import { AngularFireModule } from '@angular/fire/compat';
import { environment } from '../../../../environments/environment';
import { USE_EMULATOR as USE_FIRESTORE_EMULATOR } from '@angular/fire/compat/firestore';
import { USE_EMULATOR as USE_AUTH_EMULATOR } from '@angular/fire/compat/auth';
import { USE_EMULATOR as USE_STORAGE_EMULATOR } from '@angular/fire/compat/storage';
import { ReactiveFormsModule } from '@angular/forms';

describe('AnswerCreationModalPage', () => {
  let component: AnswerCreationModalPage;
  let fixture: ComponentFixture<AnswerCreationModalPage>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [AnswerCreationModalPage],
        imports: [
          IonicModule.forRoot(),
          ReactiveFormsModule,
          AngularFireModule.initializeApp(environment.firebase),
        ],
        providers: [
          {
            provide: USE_FIRESTORE_EMULATOR,
            useValue: environment.useEmulators
              ? ['localhost', 5000]
              : undefined,
          },
          {
            provide: USE_AUTH_EMULATOR,
            useValue: environment.useEmulators
              ? ['http://localhost', 5001]
              : undefined,
          },
          {
            provide: USE_STORAGE_EMULATOR,
            useValue: environment.useEmulators
              ? ['localhost', 5002]
              : undefined,
          },
          {
            provide: NavParams,
            useValue: { data: { carton: 'mocked carton ID' } },
          },
        ],
      }).compileComponents();

      fixture = TestBed.createComponent(AnswerCreationModalPage);
      component = fixture.componentInstance;
      fixture.detectChanges();
    })
  );

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
