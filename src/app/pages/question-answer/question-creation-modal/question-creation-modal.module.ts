import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { QuestionCreationModalPageRoutingModule } from './question-creation-modal-routing.module';

import { QuestionCreationModalPage } from './question-creation-modal.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    QuestionCreationModalPageRoutingModule,
    ReactiveFormsModule,
  ],
  declarations: [QuestionCreationModalPage],
})
export class QuestionCreationModalPageModule {}
