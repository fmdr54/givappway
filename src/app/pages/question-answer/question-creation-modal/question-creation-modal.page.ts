import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams, ToastController } from '@ionic/angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { QuestionAnswerService } from '../../../services/question-answer.service';
import { Question } from '../../../data/Question';
import { Timestamp } from '@angular/fire/firestore';
import { presentToast } from '../../../util/toast';
import { AuthenticationService } from '../../../services/authentication.service';
import { Validation } from 'src';
import { User } from '../../../data/User';

@Component({
  selector: 'app-question-creation-modal',
  templateUrl: './question-creation-modal.page.html',
  styleUrls: ['./question-creation-modal.page.scss'],
})
export class QuestionCreationModalPage implements OnInit {
  public questionForm: FormGroup;
  public questionValidationMessages: Validation;
  private user: User;

  constructor(
    private authenticationService: AuthenticationService,
    private modalController: ModalController,
    private toastController: ToastController,
    private formBuilder: FormBuilder,
    private navigationParameter: NavParams,
    private questionAnswerService: QuestionAnswerService
  ) {
    // initial loading of user
    this.user = this.authenticationService.getCurrentUser();
    // subscribe to the user observable
    this.authenticationService
      .getUserObservable()
      .subscribe((user) => (this.user = user));
    this.questionForm = this.formBuilder.group({
      question: ['', Validators.compose([Validators.required])],
    });
    this.questionValidationMessages = {
      question: [
        { type: 'required', message: 'Ein Fragentext ist erforderlich!' },
      ],
    };
  }

  ngOnInit() {}

  /**
   * Closes/dismisses this modal
   */
  public async close() {
    await this.modalController.dismiss();
  }

  /**
   * Submit handler for creating a new question.
   * Creates a new Question object and publishes it to Firestore.
   */
  public async onSubmit() {
    if (this.user === undefined) {
      return;
    }
    const questionText = this.questionForm.get('question').value;
    //create a new question
    const question = new Question(
      this.user.id,
      this.navigationParameter.data.carton,
      questionText,
      Timestamp.now()
    );
    //push it to the firestore collection
    await this.questionAnswerService.addQuestion(question);
    //dismiss the modal and present a toast
    await this.modalController.dismiss();
    await presentToast(this.toastController, 'Die Frage wurde erstellt!');
  }
}
