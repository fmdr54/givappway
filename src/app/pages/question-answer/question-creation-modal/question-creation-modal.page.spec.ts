import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule, NavParams } from '@ionic/angular';

import { QuestionCreationModalPage } from './question-creation-modal.page';
import { AngularFireModule } from '@angular/fire/compat';
import { environment } from '../../../../environments/environment';
import { USE_EMULATOR as USE_FIRESTORE_EMULATOR } from '@angular/fire/compat/firestore';
import { USE_EMULATOR as USE_AUTH_EMULATOR } from '@angular/fire/compat/auth';
import { USE_EMULATOR as USE_STORAGE_EMULATOR } from '@angular/fire/compat/storage';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';

describe('QuestionCreationModalPage', () => {
  let component: QuestionCreationModalPage;
  let fixture: ComponentFixture<QuestionCreationModalPage>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [QuestionCreationModalPage],
        imports: [
          IonicModule.forRoot(),
          ReactiveFormsModule,
          RouterTestingModule,
          AngularFireModule.initializeApp(environment.firebase),
        ],
        providers: [
          {
            provide: USE_FIRESTORE_EMULATOR,
            useValue: environment.useEmulators
              ? ['localhost', 5000]
              : undefined,
          },
          {
            provide: USE_AUTH_EMULATOR,
            useValue: environment.useEmulators
              ? ['http://localhost', 5001]
              : undefined,
          },
          {
            provide: USE_STORAGE_EMULATOR,
            useValue: environment.useEmulators
              ? ['localhost', 5002]
              : undefined,
          },
          {
            provide: NavParams,
            useValue: {
              data: {
                cartonID: 'mocked carton ID',
                question: 'mocked question iD',
              },
            },
          },
        ],
      }).compileComponents();

      fixture = TestBed.createComponent(QuestionCreationModalPage);
      component = fixture.componentInstance;
      fixture.detectChanges();
    })
  );

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
