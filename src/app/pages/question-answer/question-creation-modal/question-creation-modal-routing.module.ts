import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { QuestionCreationModalPage } from './question-creation-modal.page';

const routes: Routes = [
  {
    path: '',
    component: QuestionCreationModalPage,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class QuestionCreationModalPageRoutingModule {}
