import { Component, OnInit } from '@angular/core';
import { QuestionAnswerService } from '../../services/question-answer.service';
import { Question } from '../../data/Question';
import { AnswerCreationModalPage } from './answer-creation-modal/answer-creation-modal.page';
import {
  AlertController,
  ModalController,
  NavController,
} from '@ionic/angular';
import { QuestionCreationModalPage } from './question-creation-modal/question-creation-modal.page';
import { ActivatedRoute } from '@angular/router';
import { CartonsService } from '../../services/cartons.service';
import { User } from '../../data/User';
import { AuthenticationService } from '../../services/authentication.service';
import { showLoginAlert } from '../../util/helper';

@Component({
  selector: 'app-question-answer',
  templateUrl: './question-answer.page.html',
  styleUrls: ['./question-answer.page.scss'],
})
export class QuestionAnswerPage implements OnInit {
  public questions: Array<Question>;
  //default value needed because of async getting
  public cartonCreatorID = '';
  public user: User;
  private readonly cartonID: string;

  constructor(
    private questionAnswerService: QuestionAnswerService,
    private authenticationService: AuthenticationService,
    private cartonService: CartonsService,
    private modalController: ModalController,
    private alertController: AlertController,
    private activatedRoute: ActivatedRoute,
    private navController: NavController
  ) {
    // initial loading of user
    this.user = this.authenticationService.getCurrentUser();
    // subscribe to the user observable
    this.authenticationService
      .getUserObservable()
      .subscribe((user) => (this.user = user));
    // get the carton id from the URL
    this.cartonID = this.activatedRoute.snapshot.paramMap.get('id');
    //set the initial value by the local copy of the service
    this.questions = this.filterQuestions(
      this.questionAnswerService.getQuestions()
    );
    //subscribe the questionAnswerService to receive updates
    this.questionAnswerService
      .getObservableQuestions()
      .subscribe((questions) => {
        this.questions = this.filterQuestions(questions);
      });
    //get the creatorID from Firestore by using the service
    (async () => {
      this.cartonCreatorID = await this.cartonService.getCreatorID(
        this.cartonID
      );
    })();
  }

  /**
   * Opens the modal for creating a new question.
   */
  public async showQuestionCreationModal() {
    // if the user is not logged in, offer the possibility to login with an alert
    if (this.user === undefined) {
      await showLoginAlert('Fragen', this.alertController, this.navController);
      return;
    }
    const questionCreationModal = await this.modalController.create({
      component: QuestionCreationModalPage,
      componentProps: {
        carton: this.cartonID,
      },
      cssClass: 'questionCreationModal',
    });
    await questionCreationModal.present();
  }

  /**
   * Opens the modal for creating a new answer for a specific question.
   *
   * @param questionID ID of the question to create an answer for
   */
  public async showAnswerCreationModal(questionID: string) {
    // if the user is not logged in, offer the possibility to login with an alert
    if (this.user === undefined) {
      await showLoginAlert(
        'Antworten',
        this.alertController,
        this.navController
      );
      return;
    }
    const answerCreationModal = await this.modalController.create({
      component: AnswerCreationModalPage,
      componentProps: {
        question: questionID,
        cartonID: this.cartonID,
      },
      cssClass: 'answerCreationModal',
    });
    await answerCreationModal.present();
  }

  ngOnInit() {}

  /**
   * Filters the questions given to only those that refer to
   * the carton this page is referring to by the URL param.
   *
   * @param questions The questions to be filtered.
   * @returns Array of Questions that refer to the carton of this page.
   * @private
   */
  private filterQuestions(questions: Array<Question>) {
    return questions.filter((question) => question.carton === this.cartonID);
  }
}
