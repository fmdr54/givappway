import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { QuestionAnswerPage } from './question-answer.page';

const routes: Routes = [
  {
    path: '',
    component: QuestionAnswerPage,
  },
  {
    path: 'answer-creation-modal',
    loadChildren: () =>
      import('./answer-creation-modal/answer-creation-modal.module').then(
        (m) => m.AnswerCreationModalPageModule
      ),
  },
  {
    path: 'question-creation-modal',
    loadChildren: () =>
      import('./question-creation-modal/question-creation-modal.module').then(
        (m) => m.QuestionCreationModalPageModule
      ),
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class QuestionAnswerPageRoutingModule {}
