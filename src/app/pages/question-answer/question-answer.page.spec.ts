import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { QuestionAnswerPage } from './question-answer.page';
import { USE_EMULATOR as USE_FIRESTORE_EMULATOR } from '@angular/fire/compat/firestore';
import { environment } from '../../../environments/environment';
import { USE_EMULATOR as USE_AUTH_EMULATOR } from '@angular/fire/compat/auth';
import { USE_EMULATOR as USE_STORAGE_EMULATOR } from '@angular/fire/compat/storage';
import { AngularFireModule } from '@angular/fire/compat';
import { RouterTestingModule } from '@angular/router/testing';

describe('QuestionAnswerPage', () => {
  let component: QuestionAnswerPage;
  let fixture: ComponentFixture<QuestionAnswerPage>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [QuestionAnswerPage],
        imports: [
          IonicModule.forRoot(),
          RouterTestingModule,
          AngularFireModule.initializeApp(environment.firebase),
        ],
        providers: [
          {
            provide: USE_FIRESTORE_EMULATOR,
            useValue: environment.useEmulators
              ? ['localhost', 5000]
              : undefined,
          },
          {
            provide: USE_AUTH_EMULATOR,
            useValue: environment.useEmulators
              ? ['http://localhost', 5001]
              : undefined,
          },
          {
            provide: USE_STORAGE_EMULATOR,
            useValue: environment.useEmulators
              ? ['localhost', 5002]
              : undefined,
          },
        ],
      }).compileComponents();

      fixture = TestBed.createComponent(QuestionAnswerPage);
      component = fixture.componentInstance;
      fixture.detectChanges();
    })
  );

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
