import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../../services/authentication.service';
import { Question } from '../../data/Question';
import { QuestionAnswerService } from '../../services/question-answer.service';
import { CartonsService } from '../../services/cartons.service';
import { Router } from '@angular/router';
import { Answer } from '../../data/Answer';
import { Timestamp } from '@angular/fire/firestore';
import { User } from '../../data/User';

@Component({
  selector: 'app-my-questions',
  templateUrl: './my-questions.page.html',
  styleUrls: ['./my-questions.page.scss'],
})
export class MyQuestionsPage implements OnInit {
  public questions: Array<Question> = [];
  public answers: Array<Answer> = [];
  public filterTimestamp: Date;
  public colorTimestamp: number;
  //this is necessary since the name fetching is async, async calls in template cause
  //infinite render loops
  public questionCartonTitle: Array<string> = [];
  public answerQuestionTitle: Array<string> = [];
  public answerCartonIDs: Array<string> = [];
  private user: User = undefined;
  //marks if the timestamp was already updated
  private notificationCheckUpdated = false;

  constructor(
    private authenticationService: AuthenticationService,
    private questionAnswerService: QuestionAnswerService,
    private cartonsService: CartonsService,
    private router: Router
  ) {
    // subscribe to the user observable
    this.authenticationService.getUserObservable().subscribe(async (user) => {
      this.user = user;
      await this.updateData();
    });
    //set a snapshot listener to update the list on new questions
    //also updates the timestamp
    this.questionAnswerService
      .getObservableQuestions()
      .subscribe(async (questions) => {
        await this.updateData();
      });
  }

  /**
   * Routes the user to the question-answer page of a carton.
   *
   * @param carton The ID of the carton to route to.
   */
  public routeToCarton(carton: string) {
    this.router.navigateByUrl(`/question-answer/${carton}`);
  }

  async ngOnInit() {}

  async ionViewDidEnter() {
    //get the user from the authenticationService initially
    this.user = this.authenticationService.getCurrentUser();
    // reset the variable on entering
    this.notificationCheckUpdated = false;
    await this.updateData(true);
  }

  async ionViewWillLeave() {
    // update the timestamp of the current user to the current time before leaving
    await this.authenticationService.updateUser(
      this.user,
      this.user.street,
      this.user.postalCode,
      this.user.city,
      this.user.location,
      Timestamp.now()
    );
  }

  /**
   * Filters the questions by the timestamp - only such that are less than
   * or equal to 60 minutes before the last check timestamp will be kept.
   *
   * @param questions The questions to filter.
   * @private
   * @returns The filtered list of questions.
   */
  private async filterQuestions(
    questions: Array<Question>
  ): Promise<Array<Question>> {
    //find all cartonIDs of cartons created by the current user
    //this is more efficient than calling 'getCreatorID' on each question
    //in terms of number of firebase queries
    const cartonsByCurrentUser = await this.cartonsService.getCartonsOfUser(
      this.user.id
    );
    //filter to all questions that belong to cartons created by this user
    // and are not created by the user himself
    questions = questions.filter(
      (question) =>
        cartonsByCurrentUser.includes(question.carton) &&
        question.userID !== this.user.id
    );
    //filter to all questions that are at maximum 1 hour behind the last check
    questions = questions.filter(
      (question) =>
        question.timestamp.toDate().getTime() > this.filterTimestamp.getTime()
    );
    //get the carton names for all remaining questions and set them
    for (const question of questions) {
      const carton = await this.cartonsService.getCartonByID(question.carton);
      this.questionCartonTitle.push(carton.title);
    }
    return questions;
  }

  /**
   * Filters the answers to questions by the current user by the timestamp
   * - only such that are less than or equal to 60 minutes before the last
   * check timestamp will be kept - answers by the current user are ignored.
   *
   * @param questions The list of questions to filter with.
   * @private
   * @returns The filtered list of answers.
   */
  private async filterAnswers(
    questions: Array<Question>
  ): Promise<Array<Answer>> {
    //find all answers to questions of this user
    questions = questions.filter(
      (question) => question.userID === this.user.id
    );
    const answers: Array<Answer> = [];
    //filter to all answers of these questions that are at maximum 1 hour behind the last check
    for (const question of questions) {
      for (const answer of question.answers) {
        if (
          answer.timestamp.toDate().getTime() >
            this.filterTimestamp.getTime() &&
          answer.userID !== this.user.id
        ) {
          answers.push(answer);
        }
      }
    }
    //sort the answers by time since the service orders them differently
    answers.sort(
      (answer1, answer2) =>
        answer2.timestamp.toDate().getTime() -
        answer1.timestamp.toDate().getTime()
    );
    for (const answer of answers) {
      const question = await this.questionAnswerService.getQuestionById(
        answer.question
      );
      this.answerCartonIDs.push(question.carton);
      this.answerQuestionTitle.push(question.text);
    }
    return answers;
  }

  /**
   * Updates the displayed questions and answers by setting a new timestamp
   * based on the user and filtering questions and answers.
   *
   * @private
   */
  private async updateData(initial: boolean = false) {
    if (this.user) {
      // get the last check time and subtract 1 hour from it
      this.filterTimestamp = this.user.lastNotificationCheck.toDate();
      this.filterTimestamp.setHours(this.filterTimestamp.getHours() - 1);
      // update the timestamps only if queried
      // only update the color timestamp on the initial load (didEnter)
      if (initial) {
        // get the badge timestamp as the timestamp of the last check (for color highlighting)
        this.colorTimestamp = this.user.lastNotificationCheck
          .toDate()
          .getTime();
      }
      // dont update the check timestamp when on other pages or if it already has been
      if (
        window.location.href.includes('myQuestions') &&
        !this.notificationCheckUpdated
      ) {
        // update the timestamp of the current user to the current time
        await this.authenticationService.updateUser(
          this.user,
          this.user.street,
          this.user.postalCode,
          this.user.city,
          this.user.location,
          Timestamp.now()
        );
        this.notificationCheckUpdated = true;
      }
      // get all questions and answers
      const questions = this.questionAnswerService.getQuestions();
      //filter the answers
      this.answers = await this.filterAnswers(questions);
      //filter the questions
      this.questions = await this.filterQuestions(questions);
    }
  }
}
