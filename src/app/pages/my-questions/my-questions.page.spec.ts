import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MyQuestionsPage } from './my-questions.page';
import { AngularFireModule } from '@angular/fire/compat';
import { environment } from '../../../environments/environment';
import { USE_EMULATOR as USE_FIRESTORE_EMULATOR } from '@angular/fire/compat/firestore';
import { USE_EMULATOR as USE_AUTH_EMULATOR } from '@angular/fire/compat/auth';
import { USE_EMULATOR as USE_STORAGE_EMULATOR } from '@angular/fire/compat/storage';
import { RouterTestingModule } from '@angular/router/testing';

//TODO: reactivate when AuthenticationService is fixed
xdescribe('MyQuestionsPage', () => {
  let component: MyQuestionsPage;
  let fixture: ComponentFixture<MyQuestionsPage>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [MyQuestionsPage],
        imports: [
          IonicModule.forRoot(),
          RouterTestingModule,
          AngularFireModule.initializeApp(environment.firebase),
        ],
        providers: [
          {
            provide: USE_FIRESTORE_EMULATOR,
            useValue: environment.useEmulators
              ? ['localhost', 5000]
              : undefined,
          },
          {
            provide: USE_AUTH_EMULATOR,
            useValue: environment.useEmulators
              ? ['http://localhost', 5001]
              : undefined,
          },
          {
            provide: USE_STORAGE_EMULATOR,
            useValue: environment.useEmulators
              ? ['localhost', 5002]
              : undefined,
          },
        ],
      }).compileComponents();

      fixture = TestBed.createComponent(MyQuestionsPage);
      component = fixture.componentInstance;
      fixture.detectChanges();
    })
  );

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
