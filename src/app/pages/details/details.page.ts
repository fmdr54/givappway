import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { CartonsService } from '../../services/cartons.service';
import { Carton } from '../../data/Carton';
import SwiperCore, { SwiperOptions, Pagination, Navigation } from 'swiper';
import { StorageService } from 'src/app/services/storage.service';
import { AuthenticationService } from '../../services/authentication.service';
import { LocationService } from '../../services/location.service';
import { getPositionBySystemAPI, showLoginAlert } from '../../util/helper';
import { User } from '../../data/User';
import {
  AlertController,
  ModalController,
  NavController,
} from '@ionic/angular';
import { WithdrawalPage } from '../withdrawal/withdrawal.page';

SwiperCore.use([Navigation, Pagination]);

@Component({
  selector: 'app-details',
  templateUrl: './details.page.html',
  styleUrls: ['./details.page.scss'],
})
export class DetailsPage implements OnInit {
  public carton: Carton = undefined;
  public distance = '';
  //configuration for Swiper.js
  public swiperConfig: SwiperOptions = {
    slidesPerView: 1,
    spaceBetween: 50,
    navigation: true,
    pagination: { clickable: true },
    centeredSlides: true,
  };
  owner = false;
  private cartons: Array<Carton> = [];
  private user: User;

  constructor(
    private activatedRoute: ActivatedRoute,
    private cartonsService: CartonsService,
    private storageService: StorageService,
    private authenticationService: AuthenticationService,
    private router: Router,
    private modalController: ModalController,
    private locationService: LocationService,
    private alertController: AlertController,
    private navController: NavController
  ) {
    this.router.events.subscribe(
      (event) => event instanceof NavigationEnd && this.loadCarton()
    );
    // Subscribe to the user observable to update distance
    this.authenticationService.getUserObservable().subscribe(async (user) => {
      if (user !== undefined) {
        this.user = user;
      }
      this.distance = await this.calculateDistance(user);
      if (this.carton !== undefined && this.carton !== null) {
        this.owner = this.carton.userID === this.user?.id;
      }
    });
  }

  async ngOnInit() {}

  /**
   * Method that calculates the color to use for the displayed time. Returns the name
   * of a CSS class containing the necessary styling (to be used with [ngClass]).
   * If the current time is not in the time window of the carton, red is returned.
   * If only one hour or less is left before the time window finishes, yellow is returned.
   * In any other cases (carton currently available and more than one hour left), green is returned.
   *
   * @returns A string representing the CSS class of the color to display
   * @private
   */
  public getTimeColor() {
    if (this.carton === undefined) {
      return;
    }
    //time is calculated as UTC, getTime gives UTC milliseconds
    // (https://developer.mozilla.org/de/docs/Web/JavaScript/Reference/Global_Objects/Date/getTime)
    const currentTime = Date.now();
    //check if the time is not between start- and end-date
    if (
      currentTime < this.carton.startDate.toDate().getTime() ||
      currentTime > this.carton.endDate.toDate().getTime()
    ) {
      return 'textRed';
    }
    //1 hour = 1ms * 1000 * 60 * 60 = 3600000ms
    if (currentTime + 3600000 >= this.carton.endDate.toDate().getTime()) {
      return 'textYellow';
    }
    return 'textGreen';
  }

  /**
   * Method that generates the time window string for the view template.
   * Necessary to check if the start or end date is not on the current day,
   * will add a date information if this is the case.
   *
   * @returns A string representing the time window
   * @private
   */
  public getTimeWindow() {
    if (this.carton === undefined) {
      return;
    }
    // formatting options
    const time: Intl.DateTimeFormatOptions = {
      minute: '2-digit',
      hour: '2-digit',
    };
    const date: Intl.DateTimeFormatOptions = {
      day: 'numeric',
      month: 'numeric',
    };
    //get current date and date objects of firebase timestamps
    const currentDate = new Date();
    const startDate = this.carton.startDate.toDate();
    const endDate = this.carton.endDate.toDate();
    //generate the strings
    let startTime = startDate.toLocaleString('de-DE', time);
    //checking with the string representation ensures date, month and year are the same
    if (startDate.toDateString() !== currentDate.toDateString()) {
      startTime += ` (${startDate.toLocaleString('de-DE', date)})`;
    }
    let endTime = endDate.toLocaleString('de-DE', time);
    if (endDate.toDateString() !== currentDate.toDateString()) {
      endTime += ` (${endDate.toLocaleString('de-DE', date)})`;
    }
    return startTime + ' - ' + endTime;
  }

  /**
   * Route to /new page but giving the carton to edit as parameter
   */
  public async onEdit() {
    if (this.carton === undefined) {
      return;
    }
    await this.router.navigate(['./edit'], {
      relativeTo: this.activatedRoute,
      state: {
        ...this.carton,
        startDate: this.carton.startDate.toDate(),
        endDate: this.carton.endDate.toDate(),
      },
    });
  }

  /**
   * Opens the withdrawal modal
   */
  public async addComment() {
    if (this.carton === undefined) {
      return;
    }
    // if the user is not logged in, offer the possibility to login with an alert
    if (this.user === undefined) {
      await showLoginAlert(
        'Entnahmekommentaren',
        this.alertController,
        this.navController
      );
      return;
    }
    const modal = await this.modalController.create({
      component: WithdrawalPage,
      componentProps: {
        carton: this.carton,
      },
      cssClass: 'withdrawalModal',
    });
    await modal.present();
  }

  /**
   * Method that finds the carton for the ID requested in the URL. Sets
   * the 'carton' variable to this object to make it available for the ViewTemplate
   *
   * @private
   */
  private async selectCarton() {
    const documentID = this.activatedRoute.snapshot.paramMap.get('id');
    this.carton = this.cartons.find((carton) => carton.id === documentID);
    if (this.carton !== undefined) {
      await this.carton.loadImages(this.storageService);
    }
  }

  /**
   * Calculates the distance between user and carton by
   * fetching the user position and calculating the distance
   * with the LocationService.
   *
   * @param user The user object to calculate the distance for
   * @returns The distance in kilometers as a string
   * @private
   */
  private async calculateDistance(user: User): Promise<string> {
    if (user !== undefined && this.carton !== undefined) {
      // try to get the GeoPoint from the system API
      // if not available, use the base address of the user
      const userLocation = (await getPositionBySystemAPI()) || user.location || undefined;
      // If no location could be calculated, return an empty string
      if (userLocation === undefined) {
        return '';
      }
      return this.locationService.getDistanceFromLatLonInKm(
        userLocation.latitude,
        userLocation.longitude,
        this.carton.location.latitude,
        this.carton.location.longitude
      );
    } else {
      return '';
    }
  }

  private async loadCarton() {
    this.user = this.authenticationService.getCurrentUser();
    this.cartons = await this.cartonsService.read();
    await this.selectCarton();
    if (this.carton !== undefined && this.carton !== null) {
      this.owner = this.carton.userID === this.user?.id;
    }
    // Calculate the distance between current user and carton
    // if an active login is present
    const user = this.authenticationService.getCurrentUser();
    this.distance = await this.calculateDistance(user);
  }
}
