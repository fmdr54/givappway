import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { DetailsPageRoutingModule } from './details-routing.module';
import { SwiperModule } from 'swiper/angular';

import { DetailsPage } from './details.page';
import { TagListComponentModule } from 'src/app/components/tag-list/tag-list.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DetailsPageRoutingModule,
    SwiperModule,
    TagListComponentModule,
  ],
  declarations: [DetailsPage],
})
export class DetailsPageModule {}
