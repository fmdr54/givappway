import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';
import { DetailsPage } from './details.page';
import { RouterTestingModule } from '@angular/router/testing';
import { AngularFireModule } from '@angular/fire/compat';
import { environment } from '../../../environments/environment';
import { USE_EMULATOR as USE_FIRESTORE_EMULATOR } from '@angular/fire/compat/firestore';
import { USE_EMULATOR as USE_AUTH_EMULATOR } from '@angular/fire/compat/auth';
import { USE_EMULATOR as USE_STORAGE_EMULATOR } from '@angular/fire/compat/storage';
import { ActivatedRoute, convertToParamMap } from '@angular/router';

//also provide a firebase document to load before running this test
// Fix for param: https://stackoverflow.com/questions/49917588/angular-mock-activatedroute-using-snapshot-and-parammap
describe('DetailsPage', () => {
  let component: DetailsPage;
  let fixture: ComponentFixture<DetailsPage>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [DetailsPage],
        imports: [
          IonicModule.forRoot(),
          RouterTestingModule,
          AngularFireModule.initializeApp(environment.firebase),
        ],
        providers: [
          {
            provide: USE_FIRESTORE_EMULATOR,
            useValue: environment.useEmulators
              ? ['localhost', 5000]
              : undefined,
          },
          {
            provide: USE_AUTH_EMULATOR,
            useValue: environment.useEmulators
              ? ['http://localhost', 5001]
              : undefined,
          },
          {
            provide: USE_STORAGE_EMULATOR,
            useValue: environment.useEmulators
              ? ['localhost', 5002]
              : undefined,
          },
          {
            provide: ActivatedRoute,
            useValue: {
              snapshot: {
                paramMap: convertToParamMap({
                  id: '1',
                }),
              },
            },
          },
        ],
      }).compileComponents();

      fixture = TestBed.createComponent(DetailsPage);
      component = fixture.componentInstance;
      fixture.detectChanges();
    })
  );

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
