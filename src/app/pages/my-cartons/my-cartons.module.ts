import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MyCartonsPageRoutingModule } from './my-cartons-routing.module';

import { MyCartonsPage } from './my-cartons.page';

@NgModule({
  imports: [CommonModule, FormsModule, IonicModule, MyCartonsPageRoutingModule],
  declarations: [MyCartonsPage],
})
export class MyCartonsPageModule {}
