import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MyCartonsPage } from './my-cartons.page';

const routes: Routes = [
  {
    path: '',
    component: MyCartonsPage,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MyCartonsPageRoutingModule {}
