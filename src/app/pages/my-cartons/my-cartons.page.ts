import { Component, OnInit } from '@angular/core';
import { Carton } from 'src/app/data/Carton';
import { Router } from '@angular/router';
import { AuthenticationService } from '../../services/authentication.service';
import { CartonsService } from 'src/app/services/cartons.service';
import { User } from 'src/app/data/User';
import { StorageService } from 'src/app/services/storage.service';

@Component({
  selector: 'app-my-cartons',
  templateUrl: './my-cartons.page.html',
  styleUrls: ['./my-cartons.page.scss'],
})
export class MyCartonsPage implements OnInit {
  myCartons: Carton[] = [];
  cartons: Carton[] = [];
  user: User = undefined;

  constructor(
    private router: Router,
    private cartonsService: CartonsService,
    private authenticationService: AuthenticationService,
    private storageService: StorageService
  ) {
    // subscribe to user observable
    this.authenticationService.getUserObservable().subscribe((user) => {
      this.user = user;
      if (this.user !== undefined) {
        this.setupCartons();
      } else {
        this.myCartons = [];
      }
    });
    // subscribe cartons observable
    this.cartonsService.getObservableCartons().subscribe((cartons) => {
      this.cartons = cartons;
      this.setupCartons();
    });
  }

  async ngOnInit() {
    // Load Cartons
    this.cartons = await this.cartonsService.read();
    this.user = this.authenticationService.getCurrentUser();
    this.setupCartons();
  }

  /**
   * Routes to the Details-Page of a Carton.
   *
   * @param cartonId ID of the Carton which should be opened
   */
  public async openCarton(cartonId: string) {
    await this.router.navigate(['./details/' + cartonId]);
  }

  /**
   * Method for setting up the cartons to display.
   * Filters the cartons array by the userID and updates
   * the myCarton property if changes to the cartons of the
   * user occurred.
   *
   * @private
   */
  private setupCartons() {
    if (this.user !== undefined) {
      //Filter to display only my Cartons
      const newFilteredCartons = this.cartons.filter(
        (carton) => carton.userID === this.user.id
      );
      newFilteredCartons
        .filter(
          (carton) =>
            carton.imageReferences && carton.imageReferences.length > 0
        )
        .forEach(
          async (carton) => await carton.loadFirstImage(this.storageService)
        );
      // only update when own cartons change to avoid visual glitches
      if (this.myCartons.length !== newFilteredCartons.length) {
        this.myCartons = newFilteredCartons;
        // Load Images of Cartons
      }
    }
  }
}
