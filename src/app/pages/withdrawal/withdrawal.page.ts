import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { ModalController, NavParams, ToastController } from '@ionic/angular';
import { Carton } from 'src/app/data/Carton';
import { CartonsService } from 'src/app/services/cartons.service';
import { StorageService } from 'src/app/services/storage.service';
import { sortImageReferences, takePicture } from 'src/app/util/helper';
import { presentToast } from 'src/app/util/toast';

@Component({
  selector: 'app-withdrawal',
  templateUrl: './withdrawal.page.html',
  styleUrls: ['./withdrawal.page.scss'],
})
export class WithdrawalPage implements OnInit {
  public withdrawalForm: FormGroup;
  public validationMessages: any;
  public isUploading = false;
  public isOffline = false;
  private readonly carton: Carton;

  constructor(
    public modalController: ModalController,
    private formBuilder: FormBuilder,
    private storageService: StorageService,
    private navigationParameter: NavParams,
    private cartonService: CartonsService,
    private toastController: ToastController
  ) {
    this.withdrawalForm = this.formBuilder.group({
      passphrase: new FormControl('', [Validators.required]),
    });

    this.validationMessages = {
      passphrase: [
        { type: 'required', message: 'Die Passphrase ist erforderlich.' },
        { type: 'wrongPassphrase', message: 'Die Passphrase ist falsch.' },
        //{ type: 'offline', message: 'Du bist offline' },
      ],
    };

    this.carton = this.navigationParameter.get('carton');
  }

  ngOnInit() {
    window.addEventListener('offline', () => this.setOfflineState(true));
    window.addEventListener('online', () => this.setOfflineState(false));
  }

  /**
   * Submit handler for submitting a new withdrawal image. Checks if the passphrase
   * input of the form is correct and takes a new picture if this is the case.
   * Dismisses the modal on success
   *
   * @param withdrawalForm The form containing the passphrase input
   */
  public async onSubmit(withdrawalForm: FormGroup) {
    if (
      (withdrawalForm.get('passphrase').value as string).toLocaleLowerCase() !==
      this.carton.passphrase
    ) {
      this.withdrawalForm.get('passphrase').setErrors({
        ...this.withdrawalForm.get('passphrase').errors,
        wrongPassphrase: true,
      });
      return;
    }

    const picture = await takePicture();
    this.isUploading = true;
    try {
      const reference = await this.storageService.uploadFile(
        picture,
        `${await this.storageService.generateID()}.jpg`,
        this.carton.id,
        'withdrawalImages'
      );
      if (!this.carton.withdrawalReferences) {
        this.carton.withdrawalReferences = [];
      }
      this.carton.withdrawalReferences.push(reference);

      await this.cartonService.update(this.carton);
      this.isUploading = false;
      await presentToast(
        this.toastController,
        'Der Entnahmekommentar wurde hinzugefügt'
      );
      await this.carton.reloadLatestImage(this.storageService);
      await this.close();
    } catch (error) {
      console.error(error);
      this.isUploading = false;
      await presentToast(
        this.toastController,
        'Beim Hochladen ist ein Fehler aufgetreten. Bitte versuche es erneut.'
      );
    }
  }

  /**
   * Closes the modal by dismissing it
   */
  public async close() {
    await this.modalController.dismiss();
  }

  /**
   * Sets the 'offline' state to a new value.
   *
   * @param offline The new 'offline' state value.
   * @private
   */
  private setOfflineState(offline: boolean) {
    this.isOffline = offline;
  }
}
