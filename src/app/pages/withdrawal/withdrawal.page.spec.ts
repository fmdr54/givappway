import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { AngularFireModule } from '@angular/fire/compat';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { IonicModule, NavParams } from '@ionic/angular';
import { environment } from 'src/environments/environment';
import { USE_EMULATOR as USE_FIRESTORE_EMULATOR } from '@angular/fire/compat/firestore';
import { USE_EMULATOR as USE_AUTH_EMULATOR } from '@angular/fire/compat/auth';
import { USE_EMULATOR as USE_STORAGE_EMULATOR } from '@angular/fire/compat/storage';

import { WithdrawalPage } from './withdrawal.page';

describe('WithdrawalPage', () => {
  let component: WithdrawalPage;
  let fixture: ComponentFixture<WithdrawalPage>;
  const navParamsSpy = jasmine.createSpyObj('NavParams', ['get']);

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [WithdrawalPage],
        imports: [
          IonicModule.forRoot(),
          ReactiveFormsModule,
          RouterTestingModule,
          AngularFireModule.initializeApp(environment.firebase),
        ],
        providers: [
          {
            provide: USE_FIRESTORE_EMULATOR,
            useValue: environment.useEmulators
              ? ['localhost', 5000]
              : undefined,
          },
          {
            provide: USE_AUTH_EMULATOR,
            useValue: environment.useEmulators
              ? ['http://localhost', 5001]
              : undefined,
          },
          {
            provide: USE_STORAGE_EMULATOR,
            useValue: environment.useEmulators
              ? ['localhost', 5002]
              : undefined,
          },
          {
            provide: NavParams,
            useValue: navParamsSpy,
          },
        ],
      }).compileComponents();

      fixture = TestBed.createComponent(WithdrawalPage);
      component = fixture.componentInstance;
      fixture.detectChanges();
    })
  );

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
