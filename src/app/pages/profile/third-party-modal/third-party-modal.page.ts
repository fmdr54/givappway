import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-third-party-modal',
  templateUrl: './third-party-modal.page.html',
  styleUrls: ['./third-party-modal.page.scss'],
})
export class ThirdPartyModalPage implements OnInit {
  public thirdPartyText: string;

  constructor(private modalController: ModalController) {}

  async ngOnInit() {
    // Read the file
    const headers = new Headers();
    headers.append('Content-Type', 'text/plain; charset=UTF-8');
    const result = await fetch('assets/third-party-licenses.txt', {
      method: 'GET',
      headers,
    });
    this.thirdPartyText = await result.text();
  }

  /**
   * Closes this modal by dismissing it.
   */
  public async close() {
    await this.modalController.dismiss();
  }
}
