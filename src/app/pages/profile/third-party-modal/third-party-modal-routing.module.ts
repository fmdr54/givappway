import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ThirdPartyModalPage } from './third-party-modal.page';

const routes: Routes = [
  {
    path: '',
    component: ThirdPartyModalPage,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ThirdPartyModalPageRoutingModule {}
