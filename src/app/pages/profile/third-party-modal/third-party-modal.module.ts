import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ThirdPartyModalPageRoutingModule } from './third-party-modal-routing.module';

import { ThirdPartyModalPage } from './third-party-modal.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ThirdPartyModalPageRoutingModule,
  ],
  declarations: [ThirdPartyModalPage],
})
export class ThirdPartyModalPageModule {}
