import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../../services/authentication.service';
import {
  AlertController,
  IonInput,
  LoadingController,
  ModalController,
} from '@ionic/angular';
import { User } from '../../data/User';
import { Router } from '@angular/router';
import { LocationService } from '../../services/location.service';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { ThirdPartyModalPage } from './third-party-modal/third-party-modal.page';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {
  public user: User = undefined;
  public validationForm: FormGroup;
  validationMessages: any;

  constructor(
    private authenticationService: AuthenticationService,
    private locationService: LocationService,
    private alertController: AlertController,
    private loadingController: LoadingController,
    private modalController: ModalController,
    private router: Router,
    private formBuilder: FormBuilder
  ) {
    //initially load the user
    this.user = this.authenticationService.getCurrentUser();
    //subscribe to the user observable
    this.authenticationService
      .getUserObservable()
      .subscribe((user) => {
        this.user = user;
        // regenerate the form to load the new users address
        this.prepareFormValidation();
      });
    this.prepareFormValidation();
  }

  ngOnInit() {}

  /**
   * Method that logs a user out.
   */
  public async logOut() {
    const loading = await this.loadingController.create();
    try {
      await loading.present();
      await this.authenticationService.signOut();
      await loading.dismiss();
      await this.router.navigateByUrl('/login');
      // reset the form on successful logout
      this.validationForm.reset();
    } catch (error) {
      const alert = await this.alertController.create({
        header: 'Logout fehlgeschlagen',
        message: error.message,
        buttons: ['OK'],
      });
      await loading.dismiss();
      await alert.present();
      console.log(error);
    }
  }

  /**
   * Navigates to the 'edit-profile' page
   */
  public async navigateToEditProfile() {
    await this.router.navigate(['edit-profile']);
  }

  /**
   * Opens the modal displaying the third-party licenses
   */
  public async openThirdPartyModal() {
    const thirdPartyModal = await this.modalController.create({
      component: ThirdPartyModalPage,
    });
    await thirdPartyModal.present();
  }

  /**
   * This method updates an address of the current user.
   *
   * @param street Input element for the street
   * @param postalCode Input element for the postal code
   * @param city Input element for the city
   */
  public async updateUser(
    street: IonInput,
    postalCode: IonInput,
    city: IonInput
  ) {
    const loading = await this.loadingController.create();
    try {
      await loading.present();
      const successAlert = await this.alertController.create({
        header: 'Adresse erfolgreich aktualisiert.',
        buttons: ['OK'],
      });
      const location = await this.locationService.forwardGeocoding(
        `${street.value as string}, ${postalCode.value as string} ${
          city.value as string
        }`
      );
      await this.authenticationService.updateUser(
        this.user,
        street.value as string,
        postalCode.value as string,
        city.value as string,
        location,
        this.user.lastNotificationCheck
      );
      await loading.dismiss();
      await successAlert.present();
    } catch (error) {
      const errorAlert = await this.alertController.create({
        header: 'Änderung der Adresse fehlgeschlagen',
        message: error.message,
        buttons: ['OK'],
      });
      await loading.dismiss();
      await errorAlert.present();
    }
  }

  /**
   * Exposes the getCurrentUserEmail method from the
   * AuthenticationService to the template
   */
  public getCurrentUserEmail() {
    return this.authenticationService.getCurrentUserEmail();
  }

  /***
   * Method that returns the street of the current user.
   * The Method returns an empty string if the firebase entry of the street is not set.
   * Empty firebase string entries always return a single space.
   */
  private getUserStreet(): string {
    if (this.user === undefined || this.user.street === ' ') {
      return '';
    }
    return this.user.street;
  }

  /***
   * Method that returns the street of the current user.
   * The Method returns an empty string if the firebase entry of the street is not set.
   * Empty firebase string entries always return a single space.
   */
  private getUserPostalCode(): string {
    if (this.user === undefined || this.user.postalCode === ' ') {
      return '';
    }
    return this.user.postalCode;
  }

  /***
   * Method that returns the street of the current user.
   * The Method returns an empty string if the firebase entry of the street is not set.
   * Empty firebase string entries always return a single space.
   */
  private getUserCity(): string {
    if (this.user === undefined || this.user.city === ' ') {
      return '';
    }
    return this.user.city;
  }

  /***
   * Method that configures the validation for the email input and password input.
   */
  private prepareFormValidation() {
    this.validationForm = this.formBuilder.group({
      street: new FormControl(
        this.getUserStreet(),
        Validators.compose([Validators.required])
      ),
      postalCode: new FormControl(
        this.getUserPostalCode(),
        Validators.compose([Validators.required, Validators.pattern('[0-9]*')])
      ),
      city: new FormControl(
        this.getUserCity(),
        Validators.compose([Validators.required])
      ),
    });

    this.validationMessages = {
      street: [{ type: 'required', message: 'Straße erforderlich' }],
      postalCode: [
        { type: 'required', message: 'Postleitzahl erforderlich' },
        { type: 'pattern', message: 'Nur Zahlen erlaubt' },
      ],
      city: [{ type: 'required', message: 'Ort erforderlich' }],
    };
  }
}
