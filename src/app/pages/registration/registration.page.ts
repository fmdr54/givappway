import { Component, OnInit } from '@angular/core';
import {
  AlertController,
  IonInput,
  LoadingController,
  NavController,
} from '@ionic/angular';
import { Router } from '@angular/router';
import { AuthenticationService } from '../../services/authentication.service';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { User } from '../../data/User';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.page.html',
  styleUrls: ['./registration.page.scss'],
})
export class RegistrationPage implements OnInit {
  public registrationValidationForm: FormGroup;
  public registrationValidationMessages: any;
  private user: User;

  constructor(
    private auth: AuthenticationService,
    private navController: NavController,
    private router: Router,
    private formBuilder: FormBuilder,
    private alertController: AlertController,
    private loadingController: LoadingController
  ) {
    this.prepareFormValidation();
  }

  ngOnInit() {}

  /**
   * Method that signs up a user with email and password
   * and stores the user data to the firestore database.
   *
   * @param email The email of the new user
   * @param password The password of the new user
   */
  public async signUp(email: IonInput, password: IonInput) {
    const loading = await this.loadingController.create();

    try {
      await loading.present();
      const result = await this.auth.createUser(
        email.value as string,
        password.value as string
      );

      if (result.user.uid) {
        this.user = new User(result.user.uid, ' ', ' ', ' ', null);
        await this.auth.saveUserDetails(this.user);
        await loading.dismiss();
        await this.navController.navigateRoot('/home');
      }
    } catch (error) {
      const alert = await this.alertController.create({
        header: 'Registrierung fehlgeschlagen',
        message: error.message,
        buttons: ['OK'],
      });
      await loading.dismiss();
      await alert.present();
      console.log(error);
    }
  }

  /**
   * Method that navigates back to the login page.
   */
  public navigateToLogin(): void {
    this.navController.back();
  }

  /**
   * Method that configures the validation for the email input and password input.
   */
  private prepareFormValidation() {
    this.registrationValidationForm = this.formBuilder.group({
      email: new FormControl(
        '',
        Validators.compose([Validators.required, Validators.email])
      ),
      password: new FormControl(
        '',
        Validators.compose([Validators.required, Validators.minLength(6)])
      ),
    });

    this.registrationValidationMessages = {
      email: [
        { type: 'required', message: 'Email erforderlich' },
        { type: 'email', message: 'Dies ist keine gültige E-Mail-Adresse' },
      ],
      password: [
        { type: 'required', message: 'Passwort erforderlich' },
        {
          type: 'minlength',
          message: 'Das Passwort erfordert mindestens 6 Zeichen',
        },
      ],
    };
  }
}
