import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListContainerPage } from './list-container.page';

const routes: Routes = [
  {
    path: '',
    component: ListContainerPage,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ListContainerPageRoutingModule {}
