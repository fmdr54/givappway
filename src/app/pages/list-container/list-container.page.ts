import { Component, NgZone, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Carton } from 'src/app/data/Carton';
import { CartonsService } from 'src/app/services/cartons.service';
import { StorageService } from 'src/app/services/storage.service';
import { LocationService } from '../../services/location.service';
import { AuthenticationService } from '../../services/authentication.service';
import { GeoPoint } from '@angular/fire/firestore';
import { getPositionBySystemAPI } from '../../util/helper';
import { LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-list-container',
  templateUrl: './list-container.page.html',
  styleUrls: ['./list-container.page.scss'],
})
export class ListContainerPage implements OnInit {
  public shownCartons: Carton[];
  public searchText = '';
  public userLocation: GeoPoint = undefined;
  private cartons: Carton[];
  private currentDate = new Date();
  private currentTime = Date.now();

  constructor(
    public cartonService: CartonsService,
    private locationService: LocationService,
    private authenticationService: AuthenticationService,
    private router: Router,
    private storageService: StorageService,
    private loadingController: LoadingController,
    private zone: NgZone
  ) {
    // subscribe cartons observable
    this.cartonService.getObservableCartons().subscribe(async (cartons) => {
      this.cartons = cartons;
      // refresh the times for reloading the time values
      this.currentDate = new Date();
      this.currentTime = Date.now();
      await this.loadCartons();
    });

    // subscribe to the observable to receive updates
    this.authenticationService.getUserObservable().subscribe(async (user) => {
      if (user !== undefined && this.userLocation === undefined) {
        // try to get the GeoPoint from the system API
        // if not available, use the base address of the user
        this.userLocation = (await getPositionBySystemAPI()) || user.location || undefined;
      } else if (user === undefined) {
        // reset location on logout
        this.userLocation = undefined;
      }
    });
  }

  async ngOnInit() {
    if (!this.cartons) {
      await this.loadCartons();
    }
    //get the current user and his GeoPoint
    const currentUser = this.authenticationService.getCurrentUser();
    if (currentUser !== undefined && this.userLocation === undefined) {
      // try to get the GeoPoint from the system API
      // if not available, use the base address of the user
      this.userLocation = (await getPositionBySystemAPI()) || currentUser.location || undefined;
    }
  }

  /**
   * Method checks if the current Time is in between startDate and endDate of a Carton.
   * Depending in the Result changes the color of the displayed time.
   * If the current time is not in the time window of the carton, red is returned.
   * If only one hour or less is left before the time window finishes, yellow is returned.
   * In any other cases (carton currently available and more than one hour left), green is returned.
   *
   * @param carton Carton to be checked.
   * @returns colorstring which is used for the display.
   */
  public getTimeColor(carton: Carton): string {
    if (
      this.currentTime < carton.startDate.toDate().getTime() ||
      this.currentTime > carton.endDate.toDate().getTime()
    ) {
      return 'textRed';
    }
    //1 hour = 1ms * 1000 * 60 * 60 = 3600000ms
    if (this.currentTime + 3600000 >= carton.endDate.toDate().getTime()) {
      return 'textYellow';
    }
    return 'textGreen';
  }

  /**
   * Method that generates the time window string for the view template.
   * Necessary to check if the start or end date is not on the current day,
   * will add a date information if this is the case.
   *
   * @param carton the carton to generate the time window for
   * @returns A string representing the time window of the carton
   * @private
   */
  public getTimeWindow(carton: Carton) {
    // formatting options
    const time: Intl.DateTimeFormatOptions = {
      minute: '2-digit',
      hour: '2-digit',
    };
    const date: Intl.DateTimeFormatOptions = {
      day: 'numeric',
      month: 'numeric',
    };
    //get current date and date objects of firebase timestamps
    const currentDate = new Date();
    const startDate = carton.startDate.toDate();
    const endDate = carton.endDate.toDate();
    //generate the strings
    let startTime = startDate.toLocaleString('de-DE', time);
    //checking with the string representation ensures date, month and year are the same
    if (startDate.toDateString() !== currentDate.toDateString()) {
      startTime += ` (${startDate.toLocaleString('de-DE', date)})`;
    }
    let endTime = endDate.toLocaleString('de-DE', time);
    if (endDate.toDateString() !== currentDate.toDateString()) {
      endTime += ` (${endDate.toLocaleString('de-DE', date)})`;
    }
    return startTime + ' - ' + endTime;
  }

  /**
   * Filters all Cartons by City, Title, Tags, address, text
   *
   * @param e Event containing the text to search for
   */
  public search(e) {
    this.shownCartons = this.cartons;
    this.searchText = e.detail.value;
    this.shownCartons = this.shownCartons.filter(
      (carton) =>
        carton.address.toLowerCase().indexOf(this.searchText.toLowerCase()) >
          -1 ||
        carton.title.toLowerCase().indexOf(this.searchText.toLowerCase()) >
          -1 ||
        carton.text.toLowerCase().indexOf(this.searchText.toLowerCase()) > -1 ||
        (carton.tags &&
          carton.tags.some(
            (tag) =>
              tag.toLowerCase().indexOf(this.searchText.toLowerCase()) > -1
          ))
    );
  }

  /**
   * Routing to the DetailsPage of a Carton
   */
  public async openCarton(cartonId: string) {
    await this.router.navigateByUrl(`/details/${cartonId}`);
  }

  /**
   * Calculates the distance of a carton to the current user position
   * by using the LocationService.
   *
   * @param carton The carton to calculate the distance for.
   * @returns The distance in kilometers as a string.
   */
  public getCartonDistance(carton: Carton): string {
    if (this.userLocation !== undefined) {
      return this.locationService.getDistanceFromLatLonInKm(
        carton.location.latitude,
        carton.location.longitude,
        this.userLocation.latitude,
        this.userLocation.longitude
      );
    } else {
      return '';
    }
  }

  /**
   * Method Loads all Cartons from Service and filters them
   * Only active Cartons and Cartons in the Future are shown.
   * Cartons will be sorted by given startdate.
   * First Image will be loaded from storageService
   *
   */
  private async loadCartons() {
    // start the loading animation
    const loading = await this.loadingController.create({
      message: 'Kartons werden geladen...',
    });
    await loading.present();

    // Fetch Cartons and filter Cartons to show only active ones and Cartons in the Future
    this.cartonService.read().then(async (cartons) => {
      this.cartons = cartons.filter(
        (carton) => carton.endDate.toDate() > this.currentDate
      );

      this.shownCartons = this.cartons;

      // Sorting Cartons for startdate
      this.shownCartons = this.shownCartons.sort((a, b) => {
        if (a.startDate === b.startDate) {
          return 0;
        }
        if (a.startDate < b.startDate) {
          return -1;
        } else {
          return 1;
        }
      });
      for (const carton of this.shownCartons.filter(
        (filterCarton) =>
          filterCarton.imageReferences &&
          filterCarton.imageReferences.length > 0
      )) {
        await carton.loadFirstImage(this.storageService);
      }
      this.zone.run(() => {});
      await loading.dismiss();
    });
  }
}
