import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ListContainerPageRoutingModule } from './list-container-routing.module';

import { ListContainerPage } from './list-container.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ListContainerPageRoutingModule,
  ],
  declarations: [ListContainerPage],
})
export class ListContainerPageModule {}
