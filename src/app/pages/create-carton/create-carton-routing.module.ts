import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CreateCartonPage } from './create-carton.page';

const routes: Routes = [
  {
    path: '',
    component: CreateCartonPage,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CreateCartonPageRoutingModule {}
