import { Component, OnInit } from '@angular/core';
import { Carton } from '../../data/Carton';
import { CartonsService } from '../../services/cartons.service';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import {
  AlertController,
  LoadingController,
  ToastController,
} from '@ionic/angular';
import { presentToast } from 'src/app/util/toast';
import { Timestamp } from '@angular/fire/firestore';
import { Geolocation } from '@capacitor/geolocation';
import { geoLocationToGeoPoint, isRaining } from 'src/app/util/helper';
import { GeoPoint } from 'firebase/firestore';
import { StorageService } from 'src/app/services/storage.service';
import { takePicture } from '../../util/helper';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { NavController } from '@ionic/angular';

import SwiperCore, { SwiperOptions, Pagination, Navigation } from 'swiper';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { notEmpty } from './tags.validator';
import { User } from 'src/app/data/User';
import { LocationService } from '../../services/location.service';
import { ImageReference, LoadedImageReference, Validation } from 'src';
import { QuestionAnswerService } from 'src/app/services/question-answer.service';
SwiperCore.use([Navigation, Pagination]);

type StateCarton = Carton & {
  startDate: Date;
  endDate: Date;
};

@Component({
  selector: 'app-create-carton',
  templateUrl: './create-carton.page.html',
  styleUrls: ['./create-carton.page.scss'],
})
export class CreateCartonPage implements OnInit {
  public editMode = false;
  public cartonId: string;
  public carton?: Carton;
  public validationForm: FormGroup;
  public validationMessages: Validation;
  public lock = false;
  public isOffline = false;
  public images: File[] = [];
  //contains the blob URLs for displaying the files
  public imageURLs: LoadedImageReference[] = [];
  //configuration for Swiper.js
  public swiperConfig: SwiperOptions = {
    slidesPerView: 1,
    spaceBetween: 50,
    navigation: true,
    pagination: { clickable: true },
    centeredSlides: true,
  };
  public isRaining: -1 | 0 | 1;
  public imageUploaded: undefined | boolean = undefined;
  public uploadDisabled = false;
  private imageReferences: ImageReference[] | undefined;
  private withdrawalReferences: ImageReference[] | undefined;
  private passphrase?: string;
  private lastStartDate: string;
  private lastEndDate: string;
  private user: User;

  constructor(
    private cartonService: CartonsService,
    private authenticationService: AuthenticationService,
    private formBuilder: FormBuilder,
    private toastController: ToastController,
    private storageService: StorageService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private navController: NavController,
    private alertController: AlertController,
    private locationService: LocationService,
    private loadingController: LoadingController,
    private qaService: QuestionAnswerService
  ) {
    this.authenticationService
      .getUserObservable()
      .subscribe((user) => (this.user = user));

    const tagsFC = new FormControl([]);
    tagsFC.setValidators([notEmpty(tagsFC)]);

    this.validationForm = this.formBuilder.group({
      title: new FormControl('', Validators.compose([Validators.required])),
      startDate: new FormControl('', Validators.compose([Validators.required])),
      endDate: new FormControl('', Validators.compose([Validators.required])),
      text: new FormControl('', Validators.compose([Validators.required])),
      //tags: tagsFC, <-- not working :(
      tags: tagsFC,
    });

    this.validationForm.valueChanges.subscribe(async (data) => {
      if (
        data.endDate &&
        data.startDate &&
        (data.startDate !== this.lastStartDate ||
          data.endDate !== this.lastEndDate)
      ) {
        const location = await this.getLocation();
        if (location) {
          this.isRaining = await isRaining(
            location.latitude,
            location.longitude,
            new Date(data.startDate),
            new Date(data.endDate)
          );
        } else {
          await presentToast(
            this.toastController,
            'Wir konnten leider nicht herausfinden, ob es regnet oder nicht. Bitte erlaube den Standortzugriff, wenn Du das wissen willst.'
          );
        }
        this.lastStartDate = data.startDate;
        this.lastEndDate = data.endDate;
      }
      if (
        data.title &&
        data.startDate &&
        data.endDate &&
        data.text &&
        data.tags &&
        data.tags.length > 0 &&
        this.imageUploaded === undefined
      ) {
        this.imageUploaded = false;
      }
    });

    this.validationMessages = {
      title: [{ type: 'required', message: 'Der Titel ist erforderlich.' }],
      startDate: [
        { type: 'required', message: 'Das Start-Datum ist erforderlich.' },
      ],
      endDate: [
        { type: 'required', message: 'Das End-Datum ist erforderlich.' },
      ],
      text: [{ type: 'required', message: 'Der Text ist erforderlich.' }],
      images: [
        {
          type: 'min',
          message: 'Es muss mindestens ein Bild hochgeladen werden.',
        },
      ],
    };
  }

  async ngOnInit() {
    this.fillEditPage();
    this.user = this.authenticationService.getCurrentUser();

    window.addEventListener('offline', () => this.setOfflineState(true));
    window.addEventListener('online', () => this.setOfflineState(false));
  }

  /**
   * submits a carton form and creates a new carton in the firebase database.
   * Requests the current geolocation.
   * uploads the images to the firebase storage.
   * generates the passphrase for the carton.
   *
   * @param form form to be submitted
   * @param close if the page should be closed after creating the carton (default true)
   */
  public async onSubmit(form: FormGroup, close = true) {
    // start the loading animation (only on home)
    let loading: HTMLIonLoadingElement;
    if (window.location.href.includes('/new')) {
      loading = await this.loadingController.create({
        message: 'Erstelle Karton...',
      });
      await loading.present();
    } else if (window.location.href.includes('/edit')) {
      loading = await this.loadingController.create({
        message: 'Bearbeite Karton...',
      });
      await loading.present();
    }

    this.lock = true;
    const { title, text, startDate, endDate, tags } = form.value;

    const geoPoint = await this.getLocation();
    // if no location could be fetched, unlock and return to the form
    if (geoPoint === undefined) {
      this.lock = false;
      return;
    }
    // Translate the GeoPoint into an address
    const address = await this.locationService.reverseGeocoding(
      geoPoint.latitude,
      geoPoint.longitude
    );

    let id = this.cartonService.generateID();

    let imageRefs: ImageReference[] = (this.imageReferences || []).reverse();

    if (this.editMode && imageRefs && imageRefs.length > 0) {
      id = imageRefs[0].imageReference.split('/')[1];
    }

    if (this.images.length > 0) {
      const names = this.images.map((image) => image.name);
      imageRefs = [
        ...imageRefs,
        ...(await this.storageService.uploadFiles(this.images, names, id)),
      ];
    }

    this.carton = await Carton.create(
      this.user?.id || '#no-id-present#',
      title,
      text,
      address,
      Timestamp.fromDate(new Date(startDate)),
      Timestamp.fromDate(new Date(endDate)),
      geoPoint || new GeoPoint(0, 0),
      tags,
      (imageRefs || []).reverse(),
      this.withdrawalReferences,
      this.passphrase,
      this.cartonId
    );

    try {
      if (this.editMode) {
        try {
          await this.cartonService.update(this.carton);

          await presentToast(
            this.toastController,
            `Der Karton wurde erfolgreich bearbeitet.`
          );
        } catch (error) {
          console.error(error);

          await presentToast(
            this.toastController,
            `Der Karton konnte leider nicht bearbeitet werden.`
          );
        }
      } else {
        await this.carton.generateRandomPassphrase();
        //overwrite the cartonID used for editing mode to navigate to the url
        this.cartonId = await this.cartonService.add(this.carton);

        await presentToast(
          this.toastController,
          `Der Karton wurde erfolgreich erstellt. Denke daran, die Passphrase auf den Karton zu schreiben!`
        );
      }
      if (close) {
        await this.close();
      }
    } catch (error) {
      await presentToast(
        this.toastController,
        `Der Karton konnte nicht erstellt werden. Versuche es gleich noch einmal.`
      );
      console.error(error);
    }
    this.lock = false;

    // stop the loading animation
    if (loading !== undefined) {
      await loading.dismiss();
    }
  }

  /**
   * closing and clearing
   */
  async close() {
    await this.clear();
    if (this.editMode) {
      this.navController.setDirection('back');
      await this.router.navigate(['../'], {
        relativeTo: this.activatedRoute,
      });
    } else {
      await this.router.navigateByUrl('/details/' + this.cartonId,{
        replaceUrl: true
      });
    }
  }

  async delete() {
    const alert = await this.alertController.create({
      header: 'Bist Du sicher?',
      message:
        'Der Karton, dessen Fragen und Antworten sowie alle Bilder werden unwiderruflich gelöscht. Bist Du Dir sicher?',
      buttons: [
        {
          text: 'Abbrechen',
          role: 'cancel',
        },
        {
          // this does not work, why?
          cssClass: 'delete-button-alert',
          text: 'Löschen',
          role: 'delete',
        },
      ],
    });
    await alert.present();
    const { role } = await alert.onDidDismiss();
    if (role === 'delete' && this.cartonId) {
      const carton = await this.cartonService.getCartonByID(this.cartonId);

      const imageRefPath =
        carton.imageReferences[0] &&
        carton.imageReferences[0].imageReference
          .split('/')
          .splice(0, 2)
          .join('/');
      const withdrawRefPath =
        carton.withdrawalReferences[0] &&
        carton.withdrawalReferences[0].imageReference
          .split('/')
          .splice(0, 2)
          .join('/');

      await this.storageService.deleteFiles([
        ...carton.imageReferences,
        ...carton.withdrawalReferences,
        {
          imageReference: imageRefPath,
          timestamp: 0,
        },
        {
          imageReference: withdrawRefPath,
          timestamp: 0,
        },
      ]);

      await this.qaService.removeAll(this.cartonId);
      await this.cartonService.remove(this.cartonId);

      presentToast(this.toastController, 'Der Karton wurde gelöscht.');
      await this.clear();

      this.router.navigateByUrl('/home', {
        replaceUrl: true,
      });
    }
  }

  /**
   * Takes a picture using the {@link Camera} plugin and adds
   * it to the imageURLs array after loading it.
   */
  public async takePicture() {
    this.uploadDisabled = true;
    const timeout = 1500;
    const takeImage = async () => {
      try {
        const file = await takePicture(
          `${this.storageService.generateID()}.jpg`
        );
        this.images.push(file);
        //generate a base64 encoded string of the file to display it
        const reader = new FileReader();
        //push the base64 encoded string when its loaded
        reader.onload = (e) => {
          // string representing the image
          this.imageURLs.unshift({
            url: e.target.result as string,
            imageReference: null,
            timestamp: Timestamp.now().seconds,
          });
          this.uploadDisabled = false;
        };
        //start the reading operation
        reader.readAsDataURL(file);
        this.imageUploaded = true;
      } catch (error) {
        console.log(error);
        this.uploadDisabled = false;
      }
    };
    if (this.images?.length === 0 && this.editMode) {
      await presentToast(
        this.toastController,
        'Denke daran, nicht die Passphrase zu fotografieren!',
        timeout
      );
      setTimeout(takeImage, timeout);
      return;
    }
    takeImage();
  }

  /**
   * Deletes the image with the given index from the images array.
   *
   * @param index The index of the image to be deleted
   */
  public async deleteImage(index: number) {
    if (this.editMode) {
      // delete already uploaded images
      if (index >= this.images.length) {
        const alert = await this.alertController.create({
          header: 'Bist Du sicher?',
          message: 'Das Bild wird unwiderruflich gelöscht. Bist Du Dir sicher?',
          buttons: [
            {
              text: 'Abbrechen',
              role: 'cancel',
            },
            {
              // this does not work, why?
              cssClass: 'delete-button-alert',
              text: 'Löschen',
              role: 'delete',
            },
          ],
        });
        await alert.present();
        const { role } = await alert.onDidDismiss();
        if (role === 'delete') {
          const image = this.imageURLs[index];
          const path = image.imageReference;
          if (await this.storageService.deleteFile(image)) {
            this.imageURLs.splice(index, 1);
            if (path.includes('cartonsImages')) {
              this.imageReferences.splice(
                this.imageReferences.findIndex(
                  (ref) => ref.imageReference === path
                ),
                1
              );
              this.imageUploaded = this.imageReferences.length > 0;
            } else {
              this.withdrawalReferences.splice(
                this.withdrawalReferences.findIndex(
                  (ref) => ref.imageReference === path
                ),
                1
              );
            }
            await presentToast(
              this.toastController,
              'Das Bild wurde erfolgreich gelöscht.'
            );
            await this.onSubmit(this.validationForm, false);
          } else {
            await presentToast(
              this.toastController,
              'Das Bild wurde konnte nicht gelöscht werden. Versuche es noch einmal.'
            );
          }
        }
        return;
      }
    }
    if (index < this.images.length) {
      //splice a single element from the given index
      this.images.splice(index, 1);
      this.imageURLs.splice(index, 1);
      this.imageUploaded = this.images.length > 0;
    }
  }

  /**
   * fills the page with existing data from history.state (if present)
   *
   */
  private fillEditPage() {
    const stateCarton: StateCarton = history.state;

    if (!stateCarton || !stateCarton.id) {
      return;
    }

    this.editMode = true;

    this.validationForm.reset({
      ...stateCarton,
      startDate: stateCarton.startDate.toString(),
      endDate: stateCarton.endDate.toString(),
    });

    this.imageReferences = stateCarton.imageReferences;
    this.imageUploaded = this.imageReferences.length > 0;
    this.withdrawalReferences = stateCarton.withdrawalReferences;
    this.passphrase = stateCarton.passphrase;

    this.imageURLs = [...stateCarton.loadedImages];

    this.cartonId = stateCarton.id;
  }

  /**
   * Sets the offline state to a new value.
   *
   * @param offline The new offline state
   * @private
   */
  private setOfflineState(offline: boolean) {
    this.isOffline = offline;
  }

  /**
   * Returns the location from the system API
   * as a GeoPoint if allowed by the user.
   *
   * @returns The GeoPoint of the user or undefined if not allowed.
   * @private
   */
  private async getLocation(): Promise<GeoPoint | undefined> {
    let geoPermission = await Geolocation.checkPermissions();
    if (
      geoPermission.coarseLocation === 'prompt' ||
      geoPermission.coarseLocation === 'prompt-with-rationale' ||
      geoPermission.location === 'prompt' ||
      geoPermission.location === 'prompt-with-rationale'
    ) {
      try {
        geoPermission = await Geolocation.requestPermissions();
      } catch (error) {
        if (error.message.includes('Not implemented on web.')) {
          try {
            await Geolocation.getCurrentPosition();
          } catch (_) {}
        }
      }
    }
    if (
      geoPermission.coarseLocation === 'denied' ||
      geoPermission.coarseLocation === 'prompt-with-rationale' ||
      geoPermission.location === 'denied' ||
      geoPermission.location === 'prompt-with-rationale'
    ) {
      if (!this.user || !this.user.location) {
        await presentToast(
          this.toastController,
          'Du hast keine Standardadresse hinterlegt und auch den Zugriff auf Deinen Standort verweigert.' +
            'Eines von beiden muss erlaubt sein.'
        );
        return;
      }
      await presentToast(
        this.toastController,
        'Standort konnte nicht ermittelt werden. Gespeicherter Standort wird verwendet.'
      );
      return this.user.location;
    } else if (
      geoPermission.coarseLocation === 'granted' &&
      geoPermission.location === 'granted'
    ) {
      try {
        const result = await Geolocation.getCurrentPosition();
        return geoLocationToGeoPoint(result);
      } catch (error) {
        // catch the case of globally disabled location
        if (error.message === 'location disabled') {
          if (!this.user || !this.user.location) {
            await presentToast(
              this.toastController,
              'Du hast keine Standardadresse hinterlegt und der Standortzugriff ist nicht möglich.' +
                'Eines von beiden muss erlaubt sein.'
            );
            return;
          }
          await presentToast(
            this.toastController,
            'Standort konnte nicht ermittelt werden. Gespeicherter Standort wird verwendet.'
          );
          return this.user.location;
        }
      }
    }
    return undefined;
  }

  /**
   * Clears the form and the state of this page.
   */
  private async clear() {
    this.validationForm.reset({
      // only set tags to non-null value to enable inputs
      tags: [],
    });
    this.imageUploaded = undefined;
    [
      this.images,
      this.withdrawalReferences,
      this.imageReferences,
      this.imageURLs,
    ].forEach((value) => (value = []));
    this.isRaining = undefined;
    this.carton = undefined;
  }
}
