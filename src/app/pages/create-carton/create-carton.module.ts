import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CreateCartonPageRoutingModule } from './create-carton-routing.module';

import { CreateCartonPage } from './create-carton.page';
import { TagListComponentModule } from 'src/app/components/tag-list/tag-list.module';
import { SwiperModule } from 'swiper/angular';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CreateCartonPageRoutingModule,
    ReactiveFormsModule,
    TagListComponentModule,
    SwiperModule,
  ],
  declarations: [CreateCartonPage],
})
export class CreateCartonPageModule {}
