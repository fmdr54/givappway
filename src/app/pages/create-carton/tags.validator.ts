import {
  AbstractControl,
  FormControl,
  ValidationErrors,
  ValidatorFn,
} from '@angular/forms';

/**
 * checks if the array is not empty
 *
 * @param tags FormControl to validate
 * @returns object with error message if error occurred or undefined otherwise
 */
export const notEmpty =
  (tags: FormControl): ValidatorFn =>
  (control: AbstractControl): ValidationErrors | undefined =>
    tags.value &&
    (tags.value as string[]).length < 1 && {
      notEmpty: 'Es muss mindestens ein Tag vergeben werden',
    };
