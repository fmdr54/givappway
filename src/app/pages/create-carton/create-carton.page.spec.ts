import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CreateCartonPage } from './create-carton.page';
import { AngularFireModule } from '@angular/fire/compat';
import { environment } from '../../../environments/environment';
import { ReactiveFormsModule } from '@angular/forms';
import { USE_EMULATOR as USE_FIRESTORE_EMULATOR } from '@angular/fire/compat/firestore';
import { USE_EMULATOR as USE_AUTH_EMULATOR } from '@angular/fire/compat/auth';
import { USE_EMULATOR as USE_STORAGE_EMULATOR } from '@angular/fire/compat/storage';
import { RouterTestingModule } from '@angular/router/testing';
import { TagListComponentModule } from '../../components/tag-list/tag-list.module';

describe('CreateCartonPage', () => {
  let component: CreateCartonPage;
  let fixture: ComponentFixture<CreateCartonPage>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [CreateCartonPage],
        imports: [
          IonicModule.forRoot(),
          AngularFireModule.initializeApp(environment.firebase),
          ReactiveFormsModule,
          RouterTestingModule,
          TagListComponentModule,
        ],
        providers: [
          {
            provide: USE_FIRESTORE_EMULATOR,
            useValue: environment.useEmulators
              ? ['localhost', 5000]
              : undefined,
          },
          {
            provide: USE_AUTH_EMULATOR,
            useValue: environment.useEmulators
              ? ['http://localhost', 5001]
              : undefined,
          },
          {
            provide: USE_STORAGE_EMULATOR,
            useValue: environment.useEmulators
              ? ['localhost', 5002]
              : undefined,
          },
        ],
      }).compileComponents();

      fixture = TestBed.createComponent(CreateCartonPage);
      component = fixture.componentInstance;
      fixture.detectChanges();
    })
  );

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
