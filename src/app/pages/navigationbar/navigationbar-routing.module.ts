import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from 'src/app/guards/auth.guard';

import { NavigationbarPage } from './navigationbar.page';

const routes: Routes = [
  {
    path: '',
    component: NavigationbarPage,
    children: [
      {
        path: 'home',
        loadChildren: () =>
          import('../list-container/list-container.module').then(
            (m) => m.ListContainerPageModule
          ),
      },
      {
        path: 'new',
        loadChildren: () =>
          import('../create-carton/create-carton.module').then(
            (m) => m.CreateCartonPageModule
          ),
        canActivate: [AuthGuard],
      },
      {
        path: 'myQuestions',
        loadChildren: () =>
          import('../my-questions/my-questions.module').then(
            (m) => m.MyQuestionsPageModule
          ),
        canActivate: [AuthGuard],
      },
      {
        path: 'myCartons',
        loadChildren: () =>
          import('../my-cartons/my-cartons.module').then(
            (m) => m.MyCartonsPageModule
          ),
        canActivate: [AuthGuard],
      },
      {
        path: 'profile',
        loadChildren: () =>
          import('../profile/profile.module').then((m) => m.ProfilePageModule),
        canActivate: [AuthGuard],
      },
      {
        path: '',
        redirectTo: '/home',
        pathMatch: 'full',
      },
    ],
  },
  {
    path: '',
    redirectTo: '/home',
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NavigationbarPageRoutingModule {}
