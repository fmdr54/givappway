import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { NavigationbarPageRoutingModule } from './navigationbar-routing.module';

import { NavigationbarPage } from './navigationbar.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NavigationbarPageRoutingModule,
  ],
  declarations: [NavigationbarPage],
})
export class NavigationbarPageModule {}
