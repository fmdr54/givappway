import { Component, OnInit } from '@angular/core';
import { QuestionAnswerService } from '../../services/question-answer.service';
import { AuthenticationService } from '../../services/authentication.service';

@Component({
  selector: 'app-navigationbar',
  templateUrl: './navigationbar.page.html',
  styleUrls: ['./navigationbar.page.scss'],
})
export class NavigationbarPage implements OnInit {
  public myQuestionsBadgeCounter = 0;

  constructor(
    private questionAnswerService: QuestionAnswerService,
    private authenticationService: AuthenticationService
  ) {
    // subscribe to the badge number observable
    this.authenticationService
      .getBadgeNumberObservable()
      .subscribe((badgeNumber) => {
        this.myQuestionsBadgeCounter = badgeNumber;
      });
    //when new questions arrive, recalculate the badge number
    this.questionAnswerService
      .getObservableQuestions()
      .subscribe(async (questions) => {
        await this.authenticationService.calculateBadgeNumber(questions);
      });
    // when the user changes, recalculate the badge number
    this.authenticationService.getUserObservable().subscribe(async (user) => {
      await this.authenticationService.calculateBadgeNumber(
        this.questionAnswerService.getQuestions()
      );
    });
  }

  async ngOnInit() {}
}
