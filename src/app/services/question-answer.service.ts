import { Injectable } from '@angular/core';
import {
  AngularFirestore,
  AngularFirestoreCollection,
} from '@angular/fire/compat/firestore';
import { Answer } from '../data/Answer';
import { Question } from '../data/Question';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class QuestionAnswerService {
  //local copies of question and answers stored to avoid re-fetching when the answers need to be mapped again
  private questions: Array<Question> = [];
  private answers: Array<Answer> = [];
  //firestore collections
  private questionCollection: AngularFirestoreCollection<Question>;
  private answerCollection: AngularFirestoreCollection<Answer>;
  //only a observable for the questions is needed since the answers will be delivered with the question
  private observableQuestions: Subject<Array<Question>> = new Subject();

  constructor(private firestore: AngularFirestore) {
    this.questionCollection = firestore.collection<Question>('questions');
    this.answerCollection = firestore.collection<Answer>('answers');

    //initial value reading
    (async () => {
      this.answers = await this.readAnswers();
      this.questions = await this.readQuestions();
      Question.mapAnswersToQuestions(this.questions, this.answers);
    })();

    this.questionCollection.ref.onSnapshot((snapshot) => {
      //store the new snapshot as a local copy
      this.questions = Question.of(snapshot);
      //map the answers to the new questions
      Question.mapAnswersToQuestions(this.questions, this.answers);
      //publish the updated values via the observable
      this.observableQuestions.next(this.questions);
    });

    //orders the answers by the timestamp (asc = oldest first for list display)
    this.answerCollection.ref
      .orderBy('timestamp', 'asc')
      .onSnapshot((snapshot) => {
        //store the new snapshot as a local copy
        this.answers = Answer.of(snapshot);
        //map the answers to the new questions
        Question.mapAnswersToQuestions(this.questions, this.answers);
        //publish the updated values via the observable
        this.observableQuestions.next(this.questions);
      });
  }

  /**
   * Method that adds a new question to the Firebase collection.
   * Calls methods that remove the 'id' and 'answers' attributes since they
   * should not be contained in the Firebase documents.
   *
   * @param question The question to add to the Firebase collection
   * @returns The document ID of the newly created Firebase document
   */
  public async addQuestion(question: Question) {
    try {
      const documentReference = await this.questionCollection.add(
        question.noIDCopy()
      );
      question.id = documentReference.id;
      return question.id;
    } catch (error) {
      console.log(error);
    }
  }

  /**
   * Method that adds a new answer to the Firebase collection.
   * Calls a method that removes the 'id' attribute since it
   * should not be contained in the Firebase documents.
   *
   * @param answer The answer to add to the Firebase collection
   * @returns The document ID of the newly created Firebase document
   */
  public async addAnswer(answer: Answer) {
    try {
      const documentReference = await this.answerCollection.add(
        answer.noIDCopy()
      );
      answer.id = documentReference.id;
      return answer.id;
    } catch (error) {
      console.log(error);
    }
  }

  /**
   * Method that removes a question from the Firebase collection.
   * Also removes all answers that belong to this question.
   *
   * @param question The question to remove from the Firebase collection.
   */
  public async removeQuestion(question: Question) {
    if (question.id !== undefined) {
      //TODO: test if this works correctly if removing is implemented
      try {
        question.answers.forEach((answer) => {
          this.removeAnswer(answer);
        });
        await this.questionCollection.doc(question.id).delete();
      } catch (error) {
        console.log(error);
      }
    }
  }

  /**
   * Method that removes an answer from the Firebase collection.
   *
   * @param answer The answer to delete in Firebase.
   */
  public async removeAnswer(answer: Answer) {
    if (answer.id !== undefined) {
      try {
        await this.answerCollection.doc(answer.id).delete();
      } catch (error) {
        console.log(error);
      }
    }
  }

  /**
   * removes all questions (and answers) from a given cartonID
   *
   * @param cartonId of which all questions should be removed
   */
  public async removeAll(cartonId: string) {
    await Promise.all(
      this.getQuestions()
        .filter((question) => question.carton === cartonId)
        .map((question) => this.removeQuestion(question))
    );
  }

  /**
   * Returns the observable object to enable subscriptions to it.
   */
  public getObservableQuestions(): Subject<Array<Question>> {
    return this.observableQuestions;
  }

  /**
   * Returns the local copy of all questions.
   */
  public getQuestions(): Array<Question> {
    return this.questions;
  }

  /**
   * Returns the question with the given ID
   * or undefined if such does not exist.
   *
   * @param id The ID of the question to return the title for.
   * @returns The Question object for the given ID or undefined.
   */
  public async getQuestionById(id: string): Promise<Question> {
    if (id !== undefined && id !== null && id !== '') {
      //get the question
      const questionDocument = await this.questionCollection
        .doc(id)
        .get()
        .toPromise();
      return questionDocument
        ? Question.documentToQuestion(questionDocument)
        : undefined;
    }
  }

  /**
   * Reads the questions documents from Firebase collection,
   * and transforms them to a array of Question objects.
   *
   * @returns An array of Question objects that represent the Firebase
   * collection.
   */
  private async readQuestions(): Promise<Array<Question>> {
    try {
      const snapshot = await this.questionCollection.ref.get();
      return Question.of(snapshot);
    } catch (error) {
      console.log(error);
      return null;
    }
  }

  /**
   * Reads the answers documents from Firebase collection,
   * and transforms them to a array of Answer objects.
   *
   * @returns An array of Answer objects that represent the Firebase
   * collection.
   */
  private async readAnswers(): Promise<Array<Answer>> {
    try {
      const snapshot = await this.answerCollection.ref
        .orderBy('timestamp', 'asc')
        .get();
      return Answer.of(snapshot);
    } catch (error) {
      console.log(error);
      return null;
    }
  }
}
