import { TestBed } from '@angular/core/testing';

import { QuestionAnswerService } from './question-answer.service';
import { AngularFireModule } from '@angular/fire/compat';
import { environment } from '../../environments/environment';
import { USE_EMULATOR as USE_FIRESTORE_EMULATOR } from '@angular/fire/compat/firestore';
import { USE_EMULATOR as USE_AUTH_EMULATOR } from '@angular/fire/compat/auth';
import { USE_EMULATOR as USE_STORAGE_EMULATOR } from '@angular/fire/compat/storage';

describe('QuestionAnswerService', () => {
  let service: QuestionAnswerService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [AngularFireModule.initializeApp(environment.firebase)],
      providers: [
        {
          provide: USE_FIRESTORE_EMULATOR,
          useValue: environment.useEmulators ? ['localhost', 5000] : undefined,
        },
        {
          provide: USE_AUTH_EMULATOR,
          useValue: environment.useEmulators
            ? ['http://localhost', 5001]
            : undefined,
        },
        {
          provide: USE_STORAGE_EMULATOR,
          useValue: environment.useEmulators ? ['localhost', 5002] : undefined,
        },
      ],
    });
    service = TestBed.inject(QuestionAnswerService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
