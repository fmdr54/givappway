import { TestBed } from '@angular/core/testing';

import { CartonsService } from './cartons.service';
import { AngularFireModule } from '@angular/fire/compat';
import { environment } from '../../environments/environment';
import { Carton } from '../data/Carton';
import { Timestamp } from '@angular/fire/firestore';
import { USE_EMULATOR as USE_FIRESTORE_EMULATOR } from '@angular/fire/compat/firestore';
import { USE_EMULATOR as USE_AUTH_EMULATOR } from '@angular/fire/compat/auth';
import { USE_EMULATOR as USE_STORAGE_EMULATOR } from '@angular/fire/compat/storage';

describe('CartonsService', () => {
  let cartonsService: CartonsService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [AngularFireModule.initializeApp(environment.firebase)],
      providers: [
        {
          provide: USE_FIRESTORE_EMULATOR,
          useValue: environment.useEmulators ? ['localhost', 5000] : undefined,
        },
        {
          provide: USE_AUTH_EMULATOR,
          useValue: environment.useEmulators
            ? ['http://localhost', 5001]
            : undefined,
        },
        {
          provide: USE_STORAGE_EMULATOR,
          useValue: environment.useEmulators ? ['localhost', 5002] : undefined,
        },
      ],
    });
    cartonsService = TestBed.inject(CartonsService);
  });

  /**
   * Helper method that generates a carton in the firstore collection.
   * Can be used to set up a test that needs to interact with an existing
   * carton or add a carton in general.
   *
   * @param testName The name of the test this carton is used for. Will be
   * inserted into the carton name and description.
   * @returns the firestore document ID of the created carton
   */
  const setupCarton = async (testName: string): Promise<string> => {
    //create the test carton and add it to firestore
    const carton = await Carton.create(
      `${testName} test`,
      `A ${testName} test carton`,
      'description of the testing carton',
      'new york testing lane 13',
      Timestamp.fromDate(new Date('2022-12-24T01:00:00')),
      Timestamp.fromDate(new Date('2022-12-26T01:00:00'))
    );
    return await cartonsService.add(carton);
  };

  it('should be created', () => {
    expect(cartonsService).toBeTruthy();
  });

  //TODO: add, update and remove cartons have problems in watch test mode, fix to reactivate
  xit('should add cartons', async () => {
    const documentID = await setupCarton('add');
    //read the firestore collection
    const collection = await cartonsService.read();
    //searching for this ID should not be undefined
    const carton = collection.find(
      (collectionCarton) => collectionCarton.id === documentID
    );
    expect(carton).toBeDefined();
    //remove the carton to avoid problems with other tests
    await cartonsService.remove(carton.id);
  });

  xit('should remove cartons', async () => {
    const documentID = await setupCarton('remove');
    //read the firestore collection
    let collection = await cartonsService.read();
    //searching for this ID should not be undefined
    const carton = collection.find(
      (collectionCarton) => (collectionCarton.id = documentID)
    );
    if (!carton) {
      fail('Adding didnt work');
      // eslint-disable-next-line no-console
      console.trace();
    }
    //remove the carton
    await cartonsService.remove(carton.id);
    //the carton should now not exist anymore
    collection = await cartonsService.read();
    expect(
      collection.find((collectionCarton) => collectionCarton.id === documentID)
    ).toBeUndefined();
  });

  xit('should update cartons', async () => {
    const documentID = await setupCarton('update');
    //read the firestore collection
    let collection = await cartonsService.read();
    //searching for this ID should not be undefined
    const carton = collection.find(
      (collectionCarton) => (collectionCarton.id = documentID)
    );
    if (!carton) {
      fail('Adding didnt work');
    }
    //update the title and the text of the carton
    const oldTitle = carton.title;
    const oldText = carton.text;
    carton.title = oldTitle + ' (Update)';
    carton.text = oldText + ' (Update)';
    //push the update to the firestore document
    await cartonsService.update(carton);
    //get the carton and check if it was updated
    collection = await cartonsService.read();
    const updatedCarton = collection.find(
      (collectionCarton) => collectionCarton.id === documentID
    );
    expect(
      updatedCarton.title !== oldTitle &&
        updatedCarton.title === oldTitle + ' (Update)' &&
        updatedCarton.text !== oldText &&
        updatedCarton.text === oldText + ' (Update)'
    ).toBeTruthy();
    //remove the carton to avoid problems in other tests
    await cartonsService.remove(updatedCarton.id);
  });

  it('should not throw errors on non-existent IDs in update', async () => {
    //create a carton but don't push it to firestore
    const carton = await Carton.create(
      `Non-existent-ID test`,
      `A Non-existent-ID test carton`,
      'description of the testing carton',
      'new york testing lane 13',
      Timestamp.fromDate(new Date('2022-12-24T01:00:00')),
      Timestamp.fromDate(new Date('2022-12-26T01:00:00'))
    );
    //change to a useless id
    carton.id = 'useless and non-existent ID';
    //wait for the end of the expect before removing
    expect(async () => {
      await cartonsService.update(carton);
      await cartonsService.remove(carton.id);
    }).not.toThrow();
  });

  it('random passphrase generation should work', async () => {
    //create a new carton
    const carton = await Carton.create(
      `Passphrase test`,
      `A passphrase test carton`,
      'description of the testing carton',
      'new york testing lane 13',
      Timestamp.fromDate(new Date('2022-12-24T01:00:00')),
      Timestamp.fromDate(new Date('2022-12-26T01:00:00'))
    );
    //check if the passphrase matches the format of
    //4 words separated by dashes
    expect(carton.passphrase).toMatch(
      new RegExp('[A-Za-z0-9]*-[A-Za-z0-9]*-[A-Za-z0-9]*-[A-Za-z0-9]*')
    );
  });

  xit('observable should provide added cartons', async (done) => {
    //only a draft, it seems jasmine is not really suited to setup a subscription and trigger it afterwards
    /*let documentID = await setupCarton('observable add 1');
    console.log('added value 1');
    //register the subscriber with the expect call
    const subscription = cartonsService.getObservableCartons().subscribe(cartons => {
      console.log('trigger on observable');
      console.log(cartons.find(carton => carton.id === documentID));
      const expectedCarton = cartons.find(carton => carton.id === documentID);
      expect(expectedCarton).toBeDefined();
      //unsubscribe after the second test
      if(expectedCarton.title.includes('observable add 2')) {
        subscription.unsubscribe();
      }
      done();
    });
    //save the old documentID to remove the carton after the test
    const documentIDOld = documentID;
    documentID = await setupCarton('observable add 2');
    console.log('added value 2');
    //remove the carton to avoid problems in other tests
    const collection = await cartonsService.read();
    const cartonToRemove = collection.find(collectionCarton => collectionCarton.id === documentID);
    await cartonsService.remove(cartonToRemove);*/
  });

  xit('observable should provide updated cartons', async () => {
    expect(true).toBeTruthy();
  });

  xit('observable should update on deleted cartons', async () => {
    expect(true).toBeTruthy();
  });
});
