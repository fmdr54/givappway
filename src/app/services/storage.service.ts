import { Injectable } from '@angular/core';
import { AngularFireStorage } from '@angular/fire/compat/storage';
import {
  uploadBytes,
  ref,
  getDownloadURL,
  deleteObject,
} from '@angular/fire/storage';
import { Timestamp } from 'firebase/firestore';
import { ImageReference, LoadedImageReference } from 'src';

export type StorageNames = 'cartonsImages' | 'withdrawalImages';

@Injectable({
  providedIn: 'root',
})
export class StorageService {
  constructor(private firestorage: AngularFireStorage) {}

  /**
   * Uploads a single file to the firebase storage
   *
   * @param file File or Blob to upload
   * @param name name of the File in the Storage
   * @param storageFolder base storage folder see {@type StorageNames} for possible values
   * @param storageName subFolder in the storageFolder where the file will be stored
   * @returns the StorageReference of the uploaded file
   */
  public async uploadFile(
    file: File | Blob,
    name: string,
    storageFolder: string,
    storageName: StorageNames = 'cartonsImages'
  ): Promise<ImageReference> {
    return new Promise(async (resolve, reject) => {
      const storage = this.firestorage.storage;
      const cartonsImagesRef = ref(
        storage,
        `${storageName}/${storageFolder}/${name}`
      );
      setTimeout(() => reject('Timeout'), 10000);
      const result = await uploadBytes(cartonsImagesRef, file);
      resolve({
        imageReference: result.ref.fullPath,
        timestamp: Timestamp.now().seconds,
      });
    });
  }

  /**
   * uploads multiple files to the firebase storage in one folder
   *
   * @param files Files or Blobs to upload
   * @param names names of the Files in the Storage. Must correspond to the files order
   * @param storageFolder base storage folder see {@type StorageNames} for possible values
   * @param storageName subFolder in the storageFolder where the file will be stored
   * @returns list of StorageReferences of the uploaded files
   */
  public async uploadFiles(
    files: File[] | Blob[],
    names: string[],
    storageFolder: string,
    storageName: StorageNames = 'cartonsImages'
  ): Promise<ImageReference[]> {
    if (files.length !== names.length) {
      throw new Error('Files and names must be the same length');
    }
    const refs: ImageReference[] = [];
    for (const [index, file] of files.entries()) {
      refs.push(
        await this.uploadFile(file, names[index], storageFolder, storageName)
      );
    }
    return refs;
  }

  public generateID() {
    return this.firestorage.storage.app.firestore().collection('dummy').doc()
      .id;
  }

  /**
   * Downloads a single file from Firebase storage.
   *
   * @param reference Reference of the file to download.
   * @returns the download URL of the file.
   */
  public async downloadFile(
    reference: ImageReference
  ): Promise<LoadedImageReference> {
    if (!reference.imageReference) {
      return;
    }

    const storage = this.firestorage.storage;
    const imageRef = ref(storage, reference.imageReference);
    try {
      return { url: await getDownloadURL(imageRef), ...reference };
    } catch (error) {
      console.log(error);
      return;
    }
  }

  /**
   * Downloads multiple files by using the downloadFile method
   * for each file.
   *
   * @param references Array of file references to download.
   * @returns an array of download URLs of the provided files.
   */
  public async downloadFiles(
    references: ImageReference[]
  ): Promise<LoadedImageReference[]> {
    return Promise.all(
      references.map((reference) => this.downloadFile(reference))
    );
  }

  /**
   * Deletes a single file in the Firebase Storage.
   *
   * @param reference Reference of the file to delete.
   */
  public async deleteFile(reference: ImageReference): Promise<boolean> {
    const storage = this.firestorage.storage;
    try {
      await deleteObject(ref(storage, reference.imageReference));
      return true;
    } catch (error) {
      return false;
    }
  }

  /**
   * Deletes multiple files by calling the deleteFile method
   * for each.
   *
   * @param references Array of file references to delete.
   */
  public async deleteFiles(references: ImageReference[]): Promise<boolean[]> {
    return Promise.all(
      references.map((reference) => this.deleteFile(reference))
    );
  }
}
