import { Injectable } from '@angular/core';
import {
  AngularFirestore,
  AngularFirestoreCollection,
} from '@angular/fire/compat/firestore';
import { Carton } from '../data/Carton';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class CartonsService {
  private cartonCollection: AngularFirestoreCollection<Carton>;
  private observableCartons: Subject<Array<Carton>> = new Subject();

  constructor(private firestore: AngularFirestore) {
    this.cartonCollection = firestore.collection<Carton>('cartons');

    // subscribe the onSnapshot observable of Firebase to publish new snapshots with the local observable
    this.cartonCollection.ref.onSnapshot(async (collectionSnapshot) => {
      this.observableCartons.next(Carton.of(collectionSnapshot));
    });
  }

  /**
   * generates a new firebase id
   *
   * @returns a new firebase-like id
   */
  public generateID() {
    return this.cartonCollection.doc().ref.id;
  }

  /**
   * Method that fetches all documents from the 'Cartons' collection in
   * firebase and transforms them to a Carton object.
   *
   * @returns Promise<Array<Carton>> Promise for the array of all cartons
   * contained in the Firebase collection
   */
  public async read(): Promise<Array<Carton>> {
    try {
      const snapshot = await this.cartonCollection.get().toPromise();
      return Carton.of(snapshot);
    } catch (error) {
      console.log(error);
      return null;
    }
  }

  /**
   * Method that adds a new Carton object to the Firebase collection.
   * Sets the ID of the Carton object to the document ID assigned by Firebase.
   *
   * @param carton The Carton object to be added to the collection.
   * @returns string The ID of the created document in the Firebase collection.
   */
  public async add(carton: Carton) {
    let documentReference;
    try {
      documentReference = await this.cartonCollection.add(carton.noIDCopy());
      carton.id = documentReference.id;
      return carton.id;
    } catch (error) {
      console.log(error);
    }
  }

  /**
   * Method that removes a given Carton document from the Firestore collection.
   * ID is used for identification, if no ID is set for the Carton object, no
   * action is taken.
   *
   * @param carton The Carton object to remove from the collection.
   */
  public async remove(cartonId: string) {
    if (cartonId) {
      try {
        await this.cartonCollection.doc(cartonId).delete();
      } catch (error) {
        console.log(error.message);
      }
    }
  }

  /**
   * Method that updates a given Carton document in the Firestore collection.
   * ID is used for identification, if no ID is set for the Carton object, no
   * action is taken.
   *
   * @param carton The Carton object to remove from the collection.
   */
  public async update(carton: Carton) {
    if (carton.id !== undefined) {
      try {
        await this.cartonCollection.doc(carton.id).update(carton.noIDCopy());
      } catch (error) {
        console.log(error.message);
      }
    } else {
      throw new Error('Carton has no ID');
    }
  }

  /**
   * Returns the observable object to enable subscriptions to it.
   */
  public getObservableCartons(): Subject<Array<Carton>> {
    return this.observableCartons;
  }

  /**
   * Returns the ID of the creator of the carton that has the
   * ID given as input.
   *
   * @param cartonID the ID of the carton to get the creator for.
   * @returns the ID of the creator of the carton.
   */
  public async getCreatorID(cartonID: string): Promise<string> {
    if (!cartonID) {
      return '';
    }
    const carton = await this.cartonCollection.doc(cartonID).get().toPromise();
    const cartonData = carton.data();
    // if there is no such ID, undefined is returned
    if (cartonData === undefined) {
      throw new Error('Invalid ID');
    }
    return cartonData.userID;
  }

  /**
   * Searches all cartons created by the user with the specified ID and
   * returns a list of all IDs of this cartons.
   *
   * @param userID The user ID to search for.
   * @returns The list of IDs of the cartons by the given user.
   */
  public async getCartonsOfUser(userID: string): Promise<Array<string>> {
    const cartons = await this.read();
    const cartonsByCurrentUser = cartons.reduce((ar, carton) => {
      if (carton.userID === userID) {
        ar.push(carton.id);
      }
      return ar;
    }, new Array<string>());
    return cartonsByCurrentUser;
  }

  /**
   * Returns the carton object for a given ID or undefined if the
   * given ID does not exist in the collection.
   *
   * @param id The ID of the carton to be returned.
   * @returns The carton with the given ID.
   */
  public async getCartonByID(id: string): Promise<Carton> {
    if (id) {
      const cartonDocument = await this.cartonCollection
        .doc(id)
        .get()
        .toPromise();
      //transform the document into an object an return it
      return cartonDocument
        ? Carton.documentToCarton(cartonDocument)
        : undefined;
    }
  }
}
