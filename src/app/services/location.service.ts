import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { GeoPoint } from '@angular/fire/firestore';
import { default as axios } from 'axios';

const url = 'https://api.opencagedata.com/geocode/v1/json';

/**
 * Uses the OpenCage API: https://opencagedata.com/dashboard?chartdays=7#usage
 */
@Injectable({
  providedIn: 'root',
})
export class LocationService {
  constructor() {}

  /**
   * Uses the OpenCage API to translate a GeoPoint with
   * latitude and longitude to a textual address. Uses the first
   * of the results that features street, number, postal and city.
   * If no such exists it uses the first with postal and city.
   * If no such exists, an empty address ('-') is returned
   *
   * @param latitude The latitude of the GeoPoint to translate
   * @param longitude The longitude of the GeoPoint to translate
   * @returns A string representing the address in the format
   * '<street> <number>, <postal> <city>'
   */
  public async reverseGeocoding(
    latitude: number,
    longitude: number
  ): Promise<string> {
    try {
      const { data, status } = await axios.get(url, {
        params: {
          q: `${latitude}+${longitude}`,
          key: environment.openCage,
          language: 'de',
        },
      });
      if (status !== 200) {
        throw new Error(`Response status is ${status}`);
      }
      // Search the first result where no value is undefined (only house number allowed)
      for (const result of data.results) {
        if (
          result.components.road &&
          result.components.postcode &&
          (result.components.town || result.components.village)
        ) {
          if (result.components.house_number) {
            return (
              `${result.components.road} ${result.components.house_number},` +
              ` ${result.components.postcode} ${
                result.components.town || result.components.village
              }`
            );
          }
          return `${result.components.road}, ${result.components.postcode} ${
            result.components.town || result.components.village
          }`;
        }
      }
      // When no entry was found, use the first entry that has postal and locality
      for (const result of data.results) {
        if (
          result.components.postcode &&
          (result.components.town || result.components.village)
        ) {
          return `${result.components.postcode} ${
            result.components.town || result.components.village
          }`;
        }
      }
      // Take formatted address as last resort
      for (const result of data.results) {
        if (result.formatted) {
          return result.formatted;
        }
      }
      // When no entry was found, return an empty address
      return '-';
    } catch (error) {
      console.log(error);
    }
  }

  /**
   * Uses the OpenCage API to translate a textual address to a
   * GeoPoint with latitude and longitude.
   *
   * @param address The textual address to translate. Format should be
   * '<street> <number>, <postal> <city>'.
   * @returns A GeoPoint with the resulting latitude and longitude
   */
  public async forwardGeocoding(address: string): Promise<GeoPoint> {
    try {
      const { data, status } = await axios.get(url, {
        params: {
          q: address,
          key: environment.openCage,
          language: 'de',
        },
      });
      if (status !== 200) {
        throw new Error(`Response status is ${status}`);
      }

      return new GeoPoint(
        data.results[0].geometry.lat,
        data.results[0].geometry.lng
      );
    } catch (error) {
      console.log(error);
    }
  }

  /**
   * Calculates the distance between two GeoPoints in kilometers.
   * It is the 'as-the-crow-flies'-distance ('Luftlinie') and is
   * calculated with the implementation of the haversin formula
   * as described on https://www.movable-type.co.uk/scripts/latlong.html.
   *
   * @param lat1 Latitude of the first GeoPoint
   * @param lon1 Longitude of the first GeoPoint
   * @param lat2 Latitude of the second GeoPoint
   * @param lon2 Longitude of the second GeoPoint
   * @returns The distance between the two points in kilometers.
   */
  public getDistanceFromLatLonInKm(
    lat1: number,
    lon1: number,
    lat2: number,
    lon2: number
  ): string {
    const R = 6371; // Radius of the earth in km
    // Calculate latitudes as degree
    const dLat1 = (lat1 * Math.PI) / 180;
    const dLat2 = (lat2 * Math.PI) / 180;
    // Calculate distances/deltas as degree
    const dDeltaLatitude = ((lat2 - lat1) * Math.PI) / 180;
    const dDeltaLongitude = ((lon2 - lon1) * Math.PI) / 180;
    const a =
      Math.sin(dDeltaLatitude / 2) * Math.sin(dDeltaLatitude / 2) +
      Math.cos(dLat1) *
        Math.cos(dLat2) *
        Math.sin(dDeltaLongitude / 2) *
        Math.sin(dDeltaLongitude / 2);
    const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    const d = R * c; // Distance in km
    return d.toFixed(2).replace('.', ',');
  }
}
