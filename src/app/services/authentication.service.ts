import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { Observable, Subject } from 'rxjs';
import {
  AngularFirestore,
  AngularFirestoreCollection,
} from '@angular/fire/compat/firestore';
import { User } from '../data/User';
import { GeoPoint, Timestamp } from '@angular/fire/firestore';
import { Question } from '../data/Question';
import { Answer } from '../data/Answer';
import { CartonsService } from './cartons.service';
import { QuestionAnswerService } from './question-answer.service';
import { FirebaseAuthentication } from '@robingenz/capacitor-firebase-authentication';
import firebase from 'firebase/compat/app';
import GoogleAuthProvider = firebase.auth.GoogleAuthProvider;
import { take } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class AuthenticationService {
  isAuthenticated: Subject<boolean> = new Subject();
  private userObservable: Subject<User> = new Subject();
  private badgeNumberObservable: Subject<number> = new Subject();
  private currentUser: User = undefined;
  private currentUserEmail: string = undefined;
  private userCollection: AngularFirestoreCollection<User>;

  constructor(
    private angularFireAuth: AngularFireAuth,
    private angularFirestore: AngularFirestore,
    private cartonsService: CartonsService,
    private questionAnswerService: QuestionAnswerService
  ) {
    this.userCollection = angularFirestore.collection<User>('users');
    //subscribe to the user observable
    angularFireAuth.user.subscribe(async (user) => {
      if (user) {
        this.isAuthenticated.next(true);
        const userSnapshot = await this.userCollection.get().toPromise();
        const userDocument = userSnapshot.docs.find(
          (collectionUser) => collectionUser.id === user.uid
        );
        this.currentUserEmail = user.email;
        if (userDocument !== undefined) {
          const newUser = User.documentToObject(userDocument);
          this.userObservable.next(newUser);
          this.currentUser = newUser;
        }
      } else {
        this.currentUser = undefined;
        this.currentUserEmail = undefined;
        this.isAuthenticated.next(false);
        this.userObservable.next(undefined);
      }
    });
  }

  /**
   * Method that returns the email-address of the current user.
   */
  public getCurrentUserEmail(): string {
    return this.currentUserEmail;
  }

  /**
   * Method that returns the observable property of an object from this service class.
   */
  public getUserObservable(): Observable<User> {
    return this.userObservable;
  }

  /**
   * Method that signs in a user with his email address and password.
   *
   * @param email Email address of a user.
   * @param password Password of a user.
   */
  public signIn(email: string, password: string) {
    return this.angularFireAuth.signInWithEmailAndPassword(email, password);
  }

  /**
   * Method that signs out a user.
   */
  async signOut() {
    await this.angularFireAuth.signOut();
    // use to be able to reselect google account
    await FirebaseAuthentication.signOut();
  }

  /**
   * Method that signs in a user with his google account.
   */
  async signInWithGoogle() {
    const result = await FirebaseAuthentication.signInWithGoogle();
    /*
      On Android, the login flow only returns credentials but no user. The login
      will not be recognized by the AngularFire SDK.
      We transform the credentials to an OAuth-Token and trigger the Credential login flow
      with it on the AngulareFire SDK level to log in.
    */
    if (result.user === undefined) {
      const oAuthCredential = GoogleAuthProvider.credential(
        result.credential.idToken
      );
      return await this.angularFireAuth.signInWithCredential(oAuthCredential);
    }
    return result;
  }

  /**
   * Method that creates a new User to the firebase authentication.
   *
   * @param email Email address of a user.
   * @param password Password of a user.
   */
  public createUser(email: string, password: string) {
    return this.angularFireAuth.createUserWithEmailAndPassword(email, password);
  }

  /**
   * Method that saves userdata to the firestore database.
   *
   * @param user User data to be saved.
   */
  public async saveUserDetails(user: User) {
    try {
      await this.userCollection.doc(user.id).set(Object.assign({}, user));
      this.currentUser = user;
    } catch (error) {
      console.log(error);
    }
  }

  /**
   * Returns the currently saved user object.
   *
   * @returns The user object or null if no login is there.
   */
  public getCurrentUser() {
    return this.currentUser;
  }

  /**
   * Updates the firestore object of a given user.
   *
   * @param user User object (containing the ID)
   * @param street New street name
   * @param postalCode New postal code
   * @param city New city name
   * @param location New location coordinates
   */
  public async updateUser(
    user: User,
    street: string,
    postalCode: string,
    city: string,
    location: GeoPoint,
    lastNotificationCheck: Timestamp
  ) {
    await this.userCollection.doc(user.id).update({
      street,
      postalCode,
      city,
      location,
      lastNotificationCheck,
    });
    this.currentUser.street = street;
    this.currentUser.postalCode = postalCode;
    this.currentUser.city = city;
    this.currentUser.location = location;
    const oldTime = this.currentUser.lastNotificationCheck;
    this.currentUser.lastNotificationCheck = lastNotificationCheck;
    // recalculate the badge number of the check time changed
    if (!this.currentUser.lastNotificationCheck.isEqual(oldTime)) {
      await this.calculateBadgeNumber(
        this.questionAnswerService.getQuestions()
      );
    }
    // publish the user updates to the observable
    this.userObservable.next(this.currentUser);
  }

  /**
   * Returns the user object for a specific user ID
   *
   * @param id The id of the user to fetch to object of.
   * @returns The object of the user with the given ID.
   */
  public async getUserById(id: string) {
    if (id !== '' && id !== undefined && id !== null) {
      const user = await this.userCollection.doc(id).get().toPromise();
      return user.data() !== undefined
        ? User.documentToObject(user)
        : undefined;
    }
  }

  /**
   * Calculates the number displayed for the badge of new questions and answers
   * based on the current user (if such exists).
   *
   * @param questions The questions to filter - answers should be mapped.
   * @private
   */
  public async calculateBadgeNumber(questions: Array<Question>) {
    if (this.currentUser) {
      //get all cartons created by this user
      const cartonsByCurrentUser = await this.cartonsService.getCartonsOfUser(
        this.currentUser.id
      );
      //get the timestamp of the last check
      //TODO: when new questions are created while being on the my-questions page,
      //because differences between browsers often times the updated timestamp is too young
      // for the comparison / younger than the question
      const filterTimestamp = this.currentUser.lastNotificationCheck.toDate();
      //filter the answers
      const answers = this.filterAnswersForBadge(
        questions,
        this.currentUser.id,
        filterTimestamp
      );
      //filter to all questions that were created after the last check of the user
      //and are not created by the user himself
      questions = questions.filter(
        (question) =>
          question.timestamp.toDate().getTime() > filterTimestamp.getTime() &&
          question.userID !== this.currentUser.id &&
          cartonsByCurrentUser.includes(question.carton)
      );
      this.badgeNumberObservable.next(questions.length + answers.length);
    } else {
      this.badgeNumberObservable.next(0);
    }
  }

  /**
   * Returns the observable that publishes the badge number
   * for user notifications.
   *
   * @returns The badge number observable
   */
  public getBadgeNumberObservable() {
    return this.badgeNumberObservable;
  }

  /**
   * Waits for the next emitted value of the AngularFire user
   * observable by using RXJS pipe() and take(1) to wait for the
   * first emitted value and returns it.
   *
   * @returns the first value emitted by the user observable
   */
  public async waitForNextUserValue() {
    return await this.angularFireAuth.user.pipe(take(1)).toPromise();
  }

  /**
   * Filters the answers to questions by the given user by the timestamp
   * - only such that are less than or equal to 60 minutes before the last
   * check timestamp will be kept - answers by the user are ignored.
   *
   * @param questions The list of questions to filter with.
   * @param userID The ID of the user to filter for.
   * @param filterTimestamp The timestamp to filter by.
   * @private
   * @returns The filtered list of answers.
   */
  private filterAnswersForBadge(
    questions: Array<Question>,
    userID: string,
    filterTimestamp: Date
  ): Array<Answer> {
    //find all answers to questions of this user
    questions = questions.filter((question) => question.userID === userID);
    const answers: Array<Answer> = [];
    //filter to all answers of these questions that are at maximum 1 hour behind the last check
    questions.forEach((question) => {
      question.answers.forEach((answer) => {
        if (
          answer.timestamp.toDate().getTime() > filterTimestamp.getTime() &&
          answer.userID !== userID
        ) {
          answers.push(answer);
        }
      });
    });
    return answers;
  }
}
