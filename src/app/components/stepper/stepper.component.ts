import { Component, Input, OnInit } from '@angular/core';
import { CdkStepper } from '@angular/cdk/stepper';

@Component({
  selector: 'app-answer-stepper',
  templateUrl: './stepper.component.html',
  styleUrls: ['./stepper.component.scss'],
  providers: [{ provide: CdkStepper, useExisting: StepperComponent }],
})
/**
 * Custom stepper component based on Angular CdkStepper. Provided
 * since Ionic doesn't feature a stepper.
 * Tutorial: https://indepth.dev/posts/1284/building-a-custom-stepper-using-angular-cdk
 */
export class StepperComponent extends CdkStepper implements OnInit {
  @Input()
  activeClass = 'active';

  ngOnInit() {}
}
