import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';
import { StepperComponent } from './stepper.component';

@NgModule({
  imports: [CommonModule, FormsModule, IonicModule],
  declarations: [StepperComponent],
  exports: [StepperComponent],
})
export class StepperComponentModule {}
