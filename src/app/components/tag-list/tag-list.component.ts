import { Component, Input } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { ToastController } from '@ionic/angular';
import { presentToast } from 'src/app/util/toast';

@Component({
  selector: 'app-tag-list',
  templateUrl: './tag-list.component.html',
  styleUrls: ['./tag-list.component.scss'],
  providers: [
    {
      useExisting: TagListComponent,
      provide: NG_VALUE_ACCESSOR,
      multi: true,
    },
  ],
})
export class TagListComponent implements ControlValueAccessor {
  @Input() tags: string[] = [];
  @Input() editable = true;
  disabled = false;
  touched = false;

  constructor(private toastController: ToastController) {}

  /**
   * used for value handling
   *
   * @param obj tags to update
   */
  public writeValue(obj: string[]): void {
    this.tags = obj;
  }

  /**
   * used for value handling
   *
   * @param fn function to call when the value changes
   */
  public registerOnChange(fn: any) {
    this.onChangeFunction = fn;
  }

  /**
   * used for value handling
   *
   * @param fn function to call when the value changes
   */
  public registerOnTouched(fn: any) {
    this.onTouchedFunction = fn;
  }

  /**
   * overwrites the default enter behaviour to add a tag
   *
   * @param event keydown event
   */
  public async tagKeyDown(event: Event) {
    if (event instanceof KeyboardEvent && event.key === 'Enter') {
      const value = (event.currentTarget as HTMLIonInputElement)
        .value as string;
      // empty tags are not possible
      if (value.length === 0) {
        return;
      }
      if (this.tags.indexOf(value) === -1) {
        this.tags.push(value);
        (event.currentTarget as HTMLIonInputElement).value = '';
        this.onChangeFunction(this.tags);
      } else {
        await presentToast(
          this.toastController,
          `Der Tag '${value}' existiert bereits`
        );
      }
      event.preventDefault();
      event.stopPropagation();
    }
  }

  /**
   * removes a tag from the array by the given index
   *
   * @param index index of the tag to remove
   */
  public removeTag(index: number) {
    this.markAsTouched();
    this.tags.splice(index, 1);
    this.onChangeFunction(this.tags);
  }

  /**
   * Handler for setting the 'touched' state of the
   * component if not already set - calls the touch handler.
   */
  public markAsTouched() {
    if (!this.touched) {
      this.onTouchedFunction();
      this.touched = true;
    }
  }

  /**
   * Handler for setting the 'disabled' state of the
   * component.
   *
   * @param disabled The new 'disabled' state
   */
  public setDisabledState(disabled: boolean) {
    this.disabled = disabled;
  }

  /**
   * Necessary (because of interface)
   * handler function for changes
   *
   * @param tags The tags after the change
   */
  private onChangeFunction = (tags) => {};

  /**
   * Necessary (because of interface)
   * handler function for touch event
   */
  private onTouchedFunction = () => {};
}
