import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';
import { TagListComponent } from './tag-list.component';

@NgModule({
  imports: [CommonModule, FormsModule, IonicModule],
  declarations: [TagListComponent],
  exports: [TagListComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class TagListComponentModule {}
