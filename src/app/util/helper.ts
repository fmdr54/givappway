import { Geolocation, Position } from '@capacitor/geolocation';
import { GeoPoint } from '@firebase/firestore';
import { Camera, CameraResultType } from '@capacitor/camera';
import { default as axios } from 'axios';
import { environment } from 'src/environments/environment';
import { ImageReference } from 'src';
import { Router } from '@angular/router';
import { AlertController, NavController } from '@ionic/angular';

/**
 * Converts a Position object into a GeoPoint object.
 *
 * @param position The Position object to convert
 */
export const geoLocationToGeoPoint = (position: Position): GeoPoint =>
  new GeoPoint(position.coords.latitude, position.coords.longitude);

/**
 * takes a picture using the {@link Camera} plugin
 *
 * @param fileName filename of the new image (default 'image.jpg')
 */
export const takePicture = async (
  fileName: string = 'image.jpg'
): Promise<File> => {
  const image = await Camera.getPhoto({
    quality: 60,
    allowEditing: true,
    resultType: CameraResultType.Uri,
  });

  const imageUrl = image.webPath;
  const blob = await fetch(imageUrl).then((r) => r.blob());
  return new File([blob], fileName, {
    type: 'image/jpeg',
  });
};

/**
 * Sorts an array of imageReferences by their number.
 *
 * @param imageReferences The array of imageReferences to sort
 */
export const sortImageReferences = (imageReferences?: ImageReference[]) =>
  imageReferences
    ? imageReferences.sort((a, b) => b.timestamp - a.timestamp)
    : imageReferences;

/**
 * Returns the number of a single imageReference.
 *
 * @param imageReference The reference to return the number for
 * @returns The number as an Integer
 */
export const numberOfImageReference = (imageReference?: string) =>
  imageReference
    ? parseInt(imageReference.split('-')[1].replace('.jpg', ''), 10)
    : 0;

const weatherURL = 'https://api.openweathermap.org/data/2.5/onecall';

/**
 * checks if rain is expected within a specified date range
 *
 * @param lat latitude
 * @param lon longitude
 * @param startDate
 * @param endDate
 * @returns -1 if weather cannot be fetched for the specified date, 0 if no rain is expected, 1 if rain is expected
 */
export const isRaining = async (
  lat: number,
  lon: number,
  startDate: Date,
  endDate: Date
): Promise<-1 | 0 | 1> => {
  const sevenDays = new Date(Date.now() + 7 * 24 * 60 * 60 * 1000);
  const currentDate = new Date();
  [sevenDays, currentDate, startDate, endDate].forEach((date) =>
    resetDate(date)
  );

  if (endDate >= currentDate && startDate <= sevenDays) {
    const response = await axios.get(weatherURL, {
      params: {
        lat,
        lon,
        exclude: 'current,minutely,hourly',
        units: 'metric',
        appid: environment.openweather,
      },
    });
    const days: any[] = response.data.daily;

    return days
      .filter((day) => {
        const date = new Date(day.dt * 1000);
        resetDate(date);
        return date >= startDate && date <= endDate;
      })
      .some((day) => day.rain !== undefined || day.snow !== undefined)
      ? 1
      : 0;
  }
  return -1;
};

/**
 * Resets the hours, minutes, seconds and milliseconds
 * of a Date object to 0.
 *
 * @param date The Date object to reset.
 */
const resetDate = (date: Date) => date.setHours(0, 0, 0, 0);

/**
 * Tries to fetch the current position from the system API, returns
 * the GeoPoint if successful. If unsuccessful, it returns undefined.
 *
 * @returns The GeoPoint or undefined if not available
 */
export const getPositionBySystemAPI = async (): Promise<GeoPoint> => {
  let geoPermission = await Geolocation.checkPermissions();
  if (
    geoPermission.coarseLocation === 'prompt' ||
    geoPermission.coarseLocation === 'prompt-with-rationale' ||
    geoPermission.location === 'prompt' ||
    geoPermission.location === 'prompt-with-rationale'
  ) {
    try {
      geoPermission = await Geolocation.requestPermissions();
    } catch (error) {
      if (error.message.includes('Not implemented on web.')) {
        try {
          await Geolocation.getCurrentPosition();
        } catch (_) {}
      }
    }
  }
  if (
    geoPermission.coarseLocation === 'denied' ||
    geoPermission.coarseLocation === 'prompt-with-rationale' ||
    geoPermission.location === 'denied' ||
    geoPermission.location === 'prompt-with-rationale'
  ) {
    return undefined;
  } else if (
    geoPermission.coarseLocation === 'granted' &&
    geoPermission.location === 'granted'
  ) {
    try {
      const result = await Geolocation.getCurrentPosition();
      return geoLocationToGeoPoint(result);
    } catch (error) {
      // catch the case of globally disabled location
      if (error.message === 'location disabled') {
        return undefined;
      }
    }
  }
};

/**
 * Displays the alert that offers the user to route to the login page.
 *
 * @param functionality The name of the objects that cannot be created
 * without an active login.
 * @param alertController The AlertController to use for displaying
 * the alert.
 * @param navController The NavController to use for the routing
 */
export const showLoginAlert = async (
  functionality: 'Entnahmekommentaren' | 'Fragen' | 'Antworten',
  alertController: AlertController,
  navController: NavController
) => {
  const loginAlert = await alertController.create({
    header: 'Login notwendig',
    message: `Das Erstellen von ${functionality} erfordert einen Login.`,
    buttons: [
      {
        text: 'Zum Login',
        handler: async () => {
          await navController.navigateRoot('/login');
        },
      },
      {
        text: 'abbrechen',
        role: 'cancel',
      },
    ],
  });
  await loginAlert.present();
};
