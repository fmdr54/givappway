import { ToastController } from '@ionic/angular';

/**
 * Function to standardise the creation of a toasts.
 *
 * @param toastController the toast controller to use
 * @param message message to display
 * @param duration desired duration of the toast display
 */
export const presentToast = async (
  toastController: ToastController,
  message: string,
  duration = 2000
) => {
  const toast = await toastController.create({
    message,
    duration,
  });
  await toast.present();
};
