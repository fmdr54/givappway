import { TestBed } from '@angular/core/testing';
import { CanEditGuard } from './can-edit.guard';
import { environment } from '../../environments/environment';
import { AngularFireModule } from '@angular/fire/compat';
import { RouterTestingModule } from '@angular/router/testing';
import { USE_EMULATOR as USE_FIRESTORE_EMULATOR } from '@angular/fire/compat/firestore';
import { USE_EMULATOR as USE_AUTH_EMULATOR } from '@angular/fire/compat/auth';
import { USE_EMULATOR as USE_STORAGE_EMULATOR } from '@angular/fire/compat/storage';

describe('CanEditGuard', () => {
  let guard: CanEditGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        AngularFireModule.initializeApp(environment.firebase),
        RouterTestingModule,
      ],
      providers: [
        {
          provide: USE_FIRESTORE_EMULATOR,
          useValue: environment.useEmulators ? ['localhost', 5000] : undefined,
        },
        {
          provide: USE_AUTH_EMULATOR,
          useValue: environment.useEmulators
            ? ['http://localhost', 5001]
            : undefined,
        },
        {
          provide: USE_STORAGE_EMULATOR,
          useValue: environment.useEmulators ? ['localhost', 5002] : undefined,
        },
      ],
    });
    guard = TestBed.inject(CanEditGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
