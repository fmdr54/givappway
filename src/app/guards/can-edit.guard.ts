import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate } from '@angular/router';
import { AuthenticationService } from '../services/authentication.service';
import { CartonsService } from '../services/cartons.service';

@Injectable({
  providedIn: 'root',
})
export class CanEditGuard implements CanActivate {
  constructor(
    private authService: AuthenticationService,
    private cartonService: CartonsService
  ) {}

  /**
   * Checks if current User is the Creator of the Carton
   *
   * @param url a snapshot of the current Url to get the ID of the Carton
   * @returns true if current User is the Creator, otherwise false
   */
  async canActivate(url: ActivatedRouteSnapshot): Promise<boolean> {
    const cartonID = url.params.id;
    const userID = this.authService.getCurrentUser().id;
    const cartons = await this.cartonService.read();
    const carton = cartons.filter((c) => c.id === cartonID)[0];
    if (carton && carton.userID === userID) {
      return true;
    } else {
      return false;
    }
  }
}
