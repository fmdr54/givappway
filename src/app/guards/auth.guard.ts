import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { AuthenticationService } from '../services/authentication.service';

@Injectable({
  providedIn: 'root',
})

/**
 * This guard checks whether a user is logged in.
 * The guard navigates back to the login-page if no user is logged in.
 */
export class AuthGuard implements CanActivate {
  constructor(
    private authService: AuthenticationService,
    private router: Router,
    private alertController: AlertController
  ) {}

  /**
   * Guard method for checking if the user is currently
   * logged in and therefore allowed to enter guarded pages.
   */
  public async canActivate(): Promise<boolean> {
    if (await this.authService.waitForNextUserValue()) {
      return true;
    } else {
      await this.router.navigate(['login']);
      await this.presentAlert();
      return false;
    }
  }

  async presentAlert() {
    const alert = await this.alertController.create({
      header: 'Zugriff nicht möglich!',
      message: 'Bitte einloggen, um diese Funktion nutzen zu können.',
    });
    await alert.present();
  }
}
