import { Timestamp } from '@angular/fire/firestore';
import { Data } from './Data';
import { QuerySnapshot } from '@angular/fire/compat/firestore';

export class Answer extends Data {
  constructor(
    public userID: string,
    public question: string,
    public text: string,
    public timestamp: Timestamp,
    id?: string
  ) {
    super(id);
  }

  /**
   * Method that transforms the firebase documents to Question objects
   * by making use of the spread operator and adding the firebase ID.
   *
   * @param snapshot The snapshot containing the documents to be transformed.
   * @returns An array of all documents as Answer objects
   */
  static of(snapshot: QuerySnapshot<Answer>): Array<Answer> {
    return snapshot.docs.map((document) => {
      const answer = document.data();
      return new Answer(
        answer.userID,
        answer.question,
        answer.text,
        answer.timestamp,
        document.id
      );
    });
  }
}
