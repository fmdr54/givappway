import { GeoPoint, Timestamp } from '@angular/fire/firestore';
import { Data } from './Data';
import { QueryDocumentSnapshot } from '@angular/fire/compat/firestore';

export class User extends Data {
  constructor(
    public id: string,
    public street: string,
    public postalCode: string,
    public city: string,
    public location: GeoPoint,
    public lastNotificationCheck?: Timestamp
  ) {
    super(id);
    this.street = street;
    this.postalCode = postalCode;
    this.city = city;
    this.location = location;
    //initially all notifications will be displayed
    if (!this.lastNotificationCheck) {
      this.lastNotificationCheck = new Timestamp(0, 0);
    }
  }

  /**
   * Transforms a user document from Firestore to a User object.
   *
   * @param snapshot The document to transform to an object.
   * @returns The document as an User object
   */
  static documentToObject(snapshot: QueryDocumentSnapshot<User>): User {
    return new User(
      snapshot.id,
      snapshot.data().street,
      snapshot.data().postalCode,
      snapshot.data().city,
      snapshot.data().location,
      snapshot.data().lastNotificationCheck
    );
  }
}
