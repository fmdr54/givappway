import { Timestamp } from '@angular/fire/firestore';
import { Answer } from './Answer';
import { Data } from './Data';
import {
  QueryDocumentSnapshot,
  QuerySnapshot,
} from '@angular/fire/compat/firestore';

export class Question extends Data {
  answers: Array<Answer> = [];

  constructor(
    public userID: string,
    public carton: string,
    public text: string,
    public timestamp: Timestamp,
    id?: string
  ) {
    super(id);
  }

  /**
   * Method that transforms the firebase documents to Question objects
   * by making use of the spread operator and adding the firebase ID.
   * Also restores the empty answer array which is needed in the app but
   * not in Firebase.
   *
   * @param snapshot The snapshot containing the documents to be transformed.
   */
  static of(snapshot: QuerySnapshot<Question>): Array<Question> {
    return snapshot.docs.map((document) => this.documentToQuestion(document));
  }

  /**
   * Transforms a single Question document from Firestore into a Question object.
   *
   * @param document The document to be transformed.
   * @returns The Question object representing the document.
   */
  static documentToQuestion(
    document: QueryDocumentSnapshot<Question>
  ): Question {
    const question = document.data();
    return new Question(
      question.userID,
      question.carton,
      question.text,
      question.timestamp,
      document.id
    );
  }

  /**
   * Maps the given answers to the given questions by adding them to
   * the 'answers' array of a question if the id of the question and
   * the referred id in the answer match.
   *
   * @param questions The questions to find the answers for.
   * @param answers The answers to map to the questions
   */
  static mapAnswersToQuestions(
    questions: Array<Question>,
    answers: Array<Answer>
  ) {
    if (questions.length === 0) {
      return;
    }
    //create a map of all questions to shorten search time when assigning answers
    const questionMap = questions.reduce((map, question) => {
      //also empty the answers array, necessary for answer refresh
      question.answers = [];
      map[question.id] = question;
      return map;
    }, {});
    //add the answers to the question they belong to
    answers.forEach((answer) =>
      questionMap[answer.question].answers.push(answer)
    );
    //possible sorting of questions by newest answers
    questions.sort((question1, question2) => {
      const date2 = question2.answers[question2.answers.length - 1]
        ? question2.answers[question2.answers.length - 1].timestamp.valueOf()
        : question2.timestamp.valueOf();
      const date1 = question1.answers[question1.answers.length - 1]
        ? question1.answers[question1.answers.length - 1].timestamp.valueOf()
        : question1.timestamp.valueOf();
      return parseInt(date2, 10) - parseInt(date1, 10);
    });
  }

  /**
   * Method that creates a copy of the given question without it's answers array
   * and its ID (id is removed by super implementation).
   * This is used to remove the array before adding or updating documents
   * in the Firebase collection to not add the answers attribute to the document.
   *
   * @returns the object without the answers attribute
   */
  public noIDCopy() {
    const copy = super.noIDCopy();
    delete copy.answers;
    return copy;
  }
}
