import { QuerySnapshot } from '@angular/fire/compat/firestore';

export class Data {
  constructor(public id?: string) {
    this.id = id;
  }

  /**
   * needed as a workaround because the constructor cannot be async
   *
   * @param id id of data
   * @returns new data object
   */
  static async new(id?: string) {
    return new Data(id);
  }

  /**
   * create a new data from a snapshot
   *
   * @param snapshot Firebase Snapshot
   * @returns newly created data
   */
  static of(snapshot: QuerySnapshot<Data>): Data[] {
    return snapshot.docs.map((document) => new Data(document.id));
  }

  /**
   *
   * @returns a copy of this object without the id
   */
  public noIDCopy() {
    const copy = { ...this };
    delete copy.id;
    return copy;
  }
}
