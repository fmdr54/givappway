import { xkcdPassphrase } from 'xkcd-passphrase';
import { GeoPoint, Timestamp } from '@angular/fire/firestore';
import { StorageService } from '../services/storage.service';
import { Data } from './Data';
import {
  QueryDocumentSnapshot,
  QuerySnapshot,
} from '@angular/fire/compat/firestore';
import { ImageReference, LoadedImageReference } from 'src';

export class Carton extends Data {
  loadedImages: LoadedImageReference[] = [];

  constructor(
    public userID: string,
    public title: string,
    public text: string,
    public address: string,
    public startDate: Timestamp,
    public endDate: Timestamp,
    public location: GeoPoint,
    public tags: Array<string>,
    public imageReferences: ImageReference[],
    public withdrawalReferences?: ImageReference[],
    public passphrase?: string,
    id?: string
  ) {
    super(id);
  }

  /**
   * needed as a workaround because the constructor cannot be async
   *
   * @param userID userid
   * @param title title of carton
   * @param text text of carton
   * @param address location of carton
   * @param startDate start date of carton
   * @param endDate end date of carton
   * @param location location of carton
   * @param tags tags of carton
   * @param imageReferences image references of carton
   * @param withdrawalReferences withdrawal image references of carton
   * @param passphrase the passphrase of the carton (optional)
   * @param id firebase id of carton (optional)
   * @returns
   */
  static async create(
    userID: string,
    title: string = '',
    text: string = '',
    address: string = '',
    startDate: Timestamp,
    endDate: Timestamp,
    location: GeoPoint = new GeoPoint(0, 0),
    tags: Array<string> = [],
    imageReferences: ImageReference[] = [],
    withdrawalReferences: ImageReference[] = [],
    passphrase?: string,
    id?: string
  ) {
    const carton = new Carton(
      userID,
      title,
      text,
      address,
      startDate,
      endDate,
      location,
      tags,
      imageReferences,
      withdrawalReferences,
      passphrase,
      id
    );
    if (!passphrase) {
      await carton.generateRandomPassphrase();
    }
    return carton;
  }

  /**
   * create a new carton from a snapshot
   *
   * @param snapshot Firebase Snapshot
   * @returns newly created cartons
   */
  static of(snapshot: QuerySnapshot<Carton>): Carton[] {
    return snapshot.docs.map((document) => this.documentToCarton(document));
  }

  /** Transforms a single carton document into an carton
   * object.
   *
   * @param document The document to be transformed.
   * @returns The object representation of the carton.
   */
  static documentToCarton(document: QueryDocumentSnapshot<Carton>) {
    const carton = document.data();

    return new Carton(
      carton.userID,
      carton.title,
      carton.text,
      carton.address,
      carton.startDate,
      carton.endDate,
      carton.location,
      carton.tags,
      carton.imageReferences,
      carton.withdrawalReferences,
      carton.passphrase,
      document.id
    );
  }

  /**
   * Method to create a copy of a carton without its id and
   * loadedImages array.
   *
   * @returns a copy of this object without the id and loadedImages
   */
  noIDCopy() {
    const copy = { ...this };
    delete copy.id;
    delete copy.loadedImages;
    return copy;
  }

  /**
   * Method that generates a random passphrase to be used for
   * the carton.
   *
   * @returns string The generated passphrase as a string
   */
  public async generateRandomPassphrase() {
    this.passphrase = (await xkcdPassphrase.generateWithWordCount(4)).replace(
      /\s/g,
      '-'
    );
  }

  /**
   * load all images of the carton (except those that are already loaded)
   *
   * @param storageService StorageService instance
   */
  async loadImages(storageService: StorageService) {
    const leftImages = this.imageReferences.slice(this.loadedImages.length);
    this.loadedImages = await storageService.downloadFiles(
      [...this.withdrawalReferences, ...leftImages].sort(
        (a, b) => b.timestamp - a.timestamp
      )
    );
  }

  /**
   * load all images of the carton (except those that are already loaded)
   *
   * @param storageService StorageService instance
   */
  public async reloadLatestImage(storageService: StorageService) {
    this.loadedImages.unshift(
      await storageService.downloadFile(this.sortWithdrawals()[0])
    );
  }

  /**
   * loads only the first image
   *
   * @param storageService StorageService instance
   */
  public async loadFirstImage(storageService: StorageService) {
    this.loadedImages[0] = await storageService.downloadFile(
      [
        ...(this.imageReferences || []),
        ...(this.withdrawalReferences || []),
      ].sort((a, b) => a?.timestamp || 0 - b?.timestamp || 0)[0]
    );
  }

  /**
   * sort all withdrawals by their upload order
   *
   * @returns sorted array
   */
  private sortWithdrawals() {
    return (this.withdrawalReferences || []).sort(
      (a, b) => b.timestamp - a.timestamp
    );
  }
}
